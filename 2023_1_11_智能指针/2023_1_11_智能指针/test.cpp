#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

////RAII
//template<class T>
//class SmartPtr
//{
//public:
//	SmartPtr(T* ptr)
//		:_ptr(ptr)
//	{}
//
//	~SmartPtr()
//	{
//		cout << "delete:" << ptr << endl;
//		delete _ptr;
//	}
//
//	T& operator*()
//	{
//		return *_ptr;
//	}
//
//	T* operator->()
//	{
//		return _ptr;
//	}
//
//private:	
//	T* _ptr;
//};
//
//void Func()
//{
//	int* p1 = new int;
//	int* p2 = new int;
//	SmartPtr<int> sp1(p1);
//	SmartPtr<int> sp2(p2);
//	
//	//cout << div() << endl;
//}
//
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	
//
//	return 0;
//}

//class A
//{
//public:
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//
////private:
//	int _a1;
//	int _a2;
//};

//template<class T>
//class SmartPtr
//{
//public:
//	SmartPtr(T* ptr = nullptr) noexcept 
//		:_ptr(ptr)
//	{}
//
//	~SmartPtr() noexcept
//	{
//		cout << "delete:" << _ptr << endl;
//		delete _ptr;
//	}
//
//	T& operator*()
//	{
//		return *_ptr;
//	}
//
//	T* operator->() noexcept
//	{
//		return _ptr;
//	}
////
////private:	
////	T* _ptr;
////};
//
//namespace mr
//{
//	template<class T>
//	class auto_ptr
//	{
//	public:
//		auto_ptr(T* ptr = nullptr) noexcept
//			:_ptr(ptr)
//		{}
//
//		auto_ptr(auto_ptr<T>& ap) noexcept
//			:_ptr(ap._ptr)
//		{
//			ap._ptr = nullptr;
//		}
//
//		auto_ptr<T>& operator=(auto_ptr<T>& ap) noexcept
//		{
//			_ptr = ap._ptr;
//			ap._ptr = nullptr;
//		}
//
//		~auto_ptr() noexcept
//		{
//			cout << "delete:" << _ptr << endl;
//			delete _ptr;
//		}
//
//		T& operator*()
//		{
//			return *_ptr;
//		}
//
//		T* operator->() noexcept
//		{
//			return _ptr;
//		}
//
//	private:
//		T* _ptr;
//	};
//}
//
////int main()
////{
////	SmartPtr<pair<string, int>> sp1(new pair<string, int>("sort", 1));
////
////	cout << sp1->first << ":" << sp1->second << endl;
////
////	return 0;
////}
//
//void test_unique_ptr()
//{
//	unique_ptr<A> up1(new A);
//	//unique_ptr<A> up2(up1); //���ÿ���
//}
//
//int main()
//{
//	auto_ptr<A> ap1(new A);
//	ap1->_a1++;
//	ap1->_a2++;
//
//	auto_ptr<A> ap2(ap1);
//	ap2->_a1++;
//	ap2->_a2++;
//
//	ap1->_a1++;
//	ap1->_a2++;
// 
//	return 0;
//}

namespace mr
{
	template<class T>
	class shared_ptr
	{
	public:
		shared_ptr(T* ptr = nullptr)
			:_ptr(ptr)
			,_pcount(new int(1))
		{}

		shared_ptr(const shared_ptr<T>& sp)
			:_ptr(sp._ptr)
			,_pcount(sp._pcount)
		{
			_pcount++;
		}

		T& operator=(const shared_ptr& sp)
		{
			if (_ptr != sp._ptr)
			{
				if (--_pcount == 0)
				{
					delete _ptr;
					delete _pcount;
				}

				_ptr = sp._ptr;
				*(sp._pcount)++;
				_pcount = sp._pcount;
			}

			return this;
		}

		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

		T* get() const
		{
			return _ptr;
		}

		~shared_ptr()
		{
			if (--(*_pcount) == 0)
			{
				delete _ptr;
				delete _pcount;
			}
		}

	public:
		T* _ptr;
		int* _pcount;
	};

	template<class T>
	class weak_ptr
	{
	public:
		weak_ptr() noexcept
			:_ptr(nullptr)
		{}

		weak_ptr(const shared_ptr<T>& sp)
			:_ptr(sp.get())
		{}

		weak_ptr(const weak_ptr<T>& wp)
			:_ptr(wp._ptr)
		{}

		weak_ptr<T>& operator=(const shared_ptr<T>& sp)
		{
			_ptr = sp.get();
			return *this;
		}

		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return  _ptr;
		}

		T* get() const 
		{
			return _ptr;
		}

	public:
		T* _ptr;
	};
}

struct A
{
	int a;
	int* _ptr;

	~A()
	{
		cout << "~A()" << endl;
	}
};

template<class T>
struct DeleteArray
{
	void operator()(T* ptr)
	{
		cout << "delete[]" << endl;
		delete[] ptr;
	}
};
template<class T>
struct Free
{
	void operator()(T* ptr)
	{
		cout << "Free" << endl;
		free(ptr);
	}
};

// ����ɾ����
void shared_ptr_test1()
{
	//std::shared_ptr<A> n1(new A[5], DeleteArray<A>());
	//std::shared_ptr< A> n2(new A);

	//std::shared_ptr<int> n1(new int[5]);


	std::shared_ptr<A> n1(new A[5], [](A* ptr) {delete[] ptr; });
	std::shared_ptr< A> n2(new A);

	std::shared_ptr<int> n3(new int[5], [](int* ptr) {delete[] ptr; });

}

//int main() noexcept
//{
//	/*mr::weak_ptr<A> wp1;
//	mr::shared_ptr<A> wp2(new A);*/
//	//shared_ptr_test1();
//
//
//	return 0;
//}

int main() noexcept
{ 
	// ����1G�ڴ�
	char* p1 = new char[1024*1024*1024];
	cout << (void*)p1 << endl;

	//�ڴ�й©

	return 0;
}	