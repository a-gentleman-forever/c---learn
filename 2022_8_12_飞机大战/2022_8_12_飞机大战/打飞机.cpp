#define  _CRT_SECURE_NO_WARNINGS 1

#include "打飞机.h"

IMAGE bkimg; //背景图片
IMAGE mplane[2]; //我的飞机
IMAGE mybullet[2];  //我的子弹

int by0, by1; //背景图片y坐标
int myplanex, myplaney; //飞机坐标
bool isplay;  //是否播放背景音乐\

int t1, t2; //运行时间
int	h, m, s; //时分秒

typedef struct _Node
{
	int x;  //子弹x
	int y;	//子弹y
	struct _Node* next;
}Bullet;

Bullet* myBullet;

//初始化游戏
void InitGame()
{
	//获取电脑当前时间
	t1 = t2 = GetTickCount();
	h = m = s = 0;
	//先加载背景地图
	loadimage(&bkimg, "bg.jpg");
	//加载我的飞机
	loadimage(&mplane[0], "uiPlane0.jpg");
	loadimage(&mplane[1] , "uiPlane1.jpg");
	//加载我的子弹
	loadimage(&mybullet[0], "bullet0.jpg");
	loadimage(&mybullet[1], "bullet1.jpg");
	//背景音乐
	isplay = true;
	mciSendString("open level.mp3", 0, 0, 0);
	mciSendString("play level.mp3", 0, 0, 0);
	//背景地图y坐标初始化
	by0 = -HEIGHT;
	by1 = 0;

	//飞机坐标初始化
	myplanex = (WIDTH - 60) / 2;
	myplaney = HEIGHT - 62;

	//初始化子弹链表
	myBullet = nullptr;
}

//绘制游戏
void DrawGame()
{
	BeginBatchDraw();
	//输出地图
	putimage(0, by0, &bkimg);
	putimage(0, by1, &bkimg);
	
	//输出我的飞机
	putimage(myplanex, myplaney, &mplane[0], SRCPAINT);
	putimage(myplanex, myplaney, &mplane[1], SRCAND);

	//输出背景音乐的状态
	if (isplay == true)
	{
		outtextxy(5, 5, "OFF");
	}
	else
	{
		outtextxy(5, 5, "ON");
	}

	//绘制游戏运行时间
	char str[100] = { 0 };
	sprintf(str, "%02d:%02d:%02d", h, m, s);
	outtextxy(450, 5, str);

	//遍历子弹
	Bullet* p = myBullet;
	while (p) 
	{
		putimage(p->x, p->y, &mybullet[0], SRCPAINT);
		putimage(p->x, p->y, &mybullet[1], SRCAND);

		p->y -= 5;

		p = p->next;
	}

	EndBatchDraw();
}

//更新游戏
void UpdateGame()
{
	//获取用户按键信息
	ExMessage msg;
	peekmessage(&msg, EM_KEY |	EM_MOUSE);

	switch (msg.message)
	{
		case WM_LBUTTONDOWN:
			if (msg.x >= 5 && msg.x <= 40 && msg.y >= 5 && msg.y <= 35)
			{		
				if (isplay == true)
				{
					mciSendString("pause level.mp3", 0, 0, 0);
				}
				else
				{
					mciSendString("resume level.mp3", 0, 0, 0);
				}

				isplay = !isplay;
			}
		break;
		case WM_KEYDOWN:
		{
			switch (msg.vkcode)
			{
			case VK_SPACE:
				CreatBullet();
				break;
			case VK_UP://上键
				myplaney -= SPEED;
				if (myplaney < 0)
				{
					myplaney = 0;
				}
				break;
			case VK_DOWN://下键
				myplaney += SPEED;
				if (myplaney > HEIGHT - 62)
				{
					myplaney = HEIGHT - 62;
				}
				break;
				break;
			case VK_LEFT://左键
				myplanex -= SPEED;
				if (myplanex < -30)
				{
					myplanex = -30;
				}
				break;
			case VK_RIGHT://右键
				myplanex += SPEED;
				if (myplanex > WIDTH - 30)
				{
					myplanex = WIDTH - 30;
				}
				break;
			}
		break;
		}
	}

	//获取电脑当前时间
	t2 = GetTickCount();
	if (t2 - t1 > 1000)
	{
		s++;
		if (s >= 60)
		{
			m++;
			s = 0;
			if (m >= 60)
			{
				h++;
				m = 0;
			}
		}
		t1 = t2;
	}

	by0++;
	by1++;

	if (by0 > HEIGHT)
	{
		by0 = -HEIGHT;
	}
	if (by1 > HEIGHT)
	{
		by1 = -HEIGHT;
	}

	Sleep(10);
}

//创建子弹
void CreatBullet()
{
	Bullet* pNewBullet = new Bullet;
	pNewBullet->x = myplanex + 25;
	pNewBullet->y = myplaney;
	pNewBullet->next = nullptr;

	if (myBullet == nullptr)
	{
		myBullet = pNewBullet;
	}
	else
	{
		pNewBullet->next = myBullet;
		myBullet = pNewBullet;
	}
}