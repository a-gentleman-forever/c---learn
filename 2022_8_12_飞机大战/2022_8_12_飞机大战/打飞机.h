#pragma once

#include <stdio.h>
#include <easyx.h>
#include <stdlib.h>
#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")

#define WIDTH 520
#define HEIGHT 750

#define SPEED 15 //飞机速度

//初始化游戏
void InitGame();

//绘制游戏
void DrawGame();

//更新游戏
void UpdateGame();

//创建子弹
void CreatBullet();
