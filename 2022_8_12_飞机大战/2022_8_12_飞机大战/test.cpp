#define  _CRT_SECURE_NO_WARNINGS 1

#include "打飞机.h"

	/*
	
	飞机大战: 准备一个窗口 准备一个地图 飞机 飞机的子弹 敌机的子弹 加血包 无敌包 BOSS BOSS的子弹 护盾 血条
	*/

int main()
{
	/*1.创建一个窗口*/
	initgraph(WIDTH, HEIGHT);

	/*2.初始化游戏*/
	InitGame();

	while (1)
	{
		/*3.绘制游戏*/
		DrawGame();

		/*4.更新游戏*/
		UpdateGame();
	}

	return 0;
}