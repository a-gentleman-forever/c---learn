#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

// bool  占用一个字节
int main()
{
	// 创建布尔类型
	bool flag = true;

	cout << flag << endl;
	cout << sizeof(bool) << endl;

	system("pause");

	return 0;
}