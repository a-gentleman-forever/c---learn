#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

//class B
//{
//public:
//	B()
//	{
//		_b = 1;
//	}
//private:
//	int _b;
//};
//
//class A
//{
//public:
//	//不提供显示构造函数
//private:
//	int _a;
//	B _c;
//};
//
//int main()
//{
//	A a1;
//
//	return 0;
//}

//class A
//{
//public:
//	A(int a = 10, int b = 20)
//	{
//		_a = a;
//		_b = b;
//	}
//private:
//	int _a;
//	int _b;
//};
//
//int main()
//{
//	A a;
//	A a1(11, 22);
//
//	return 0;
//}
//
//class A
//{
//public:
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//private:
//	int _a;
//};
//
//void test01()
//{
//	A a;
//}
//
//int main()
//{
//	test01();
//
//	return 0;
//}

//class A
//{
//public:
//	A(int a = 0)
//	{
//		_a = a;
//	}
//
//	A(const A& a)
//	{
//		_a = a._a;
//		cout << "A(const A&)" << endl;
//	}
//
//private:
//	int _a;
//};
//
//int main()
//{
//	A a1(10);
//	A a2(a1);
//	A a3 = a1;
//
//	return 0;
//}
//
//class A
//{
//public:
//	A(int a = 10, int b = 20)
//	{
//		_a = a;
//		_b = b;
//	}
//
//	void Print() const //隐藏的指针为 const A* const this
//	{
//
//	}
//
//	const A& operator=(const A& a2)
//	{
//		_a = a2._a;
//		_b = a2._b;
//		cout << "operator=(const A&)" << endl;
//
//		return a2;
//	}
//
//private:
//	int _a;
//	int _b;
//};
//
//int main()
//{
//	A a1(100, 200);
//	A a2;
//	a2 = a1;
//
//	return 0;
//}
//
//class A
//{
//public:
//	explicit A(int a = 10)
//		:_a(a)
//	{
//
//	}
//private:
//	int _a = 10;
//	int _b = 20;
//};
//
//int main()
//{
//	A a1(100);
//	A a2 = 200;
//
//	return 0;
//}

//class A
//{
//public:
//	A()
//	{
//		cout << _b << endl;
//	}
//
//	static void Print()
//	{
//		cout << _b << endl;
//		//cout << _a << endl;
//	}
//
//private:
//	int _a;
//	static int _b;
//};
//
//int A::_b = 10;
//
//int main()
//{
//	A a1;
//
//	a1.Print();
//
//	return 0;
//}

class 类
{
public:
	类(int a = 10)
		:哈哈(a)
	{

	}

	void 打印()
	{
		cout << 哈哈 << endl;
	}
private:
	int 哈哈;
};

int main()
{
	类 安在;
	
	安在.打印();

	return 0;
}