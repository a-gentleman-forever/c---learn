#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

int main()
{
	int num = 1;

	
	do
	{
		int sum = 0;
		int tmp = num;
		while (tmp)
		{
			int n = tmp % 10;
			sum += n*n*n; 

			tmp /= 10;
		}

		if (sum == num)
		{
			cout << num << endl;
		}

		num++;
	} while (num < 1000);

	
	system("pause");
	
	return 0;
}