#define  _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <easyx.h>
#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")

int main()
{
	//1.创建窗口
	//EW_SHOWCONTROL 显示控制台窗口
	initgraph(1200, 800);
	
	//2.坐标

	//3.颜色
	//a.宏定义常量表示颜色：WHITE BLACK RED YELLOW  BLUE
	//b.用宏RGB合成一个颜色: RGB(red, green, blue) 0->255

	//setbkcolor(YELLOW);
	//setbkcolor(RGB(255, 174, 201));
	setbkcolor(RGB(255, 0, 0));

	//4.刷新

	cleardevice();

	//5.music
	//Unicode字符集 前面加L
	//多字节字符集：不需要
	//mciSendString(L"open LBI利比-小城夏天.mp3", NULL, 0, NULL);
	//mciSendString(L"play LBI利比-小城夏天.mp3", NULL, 0, NULL);
	//Sleep(3000);
	////停
	//mciSendString(L"pause LBI利比-小城夏天.mp3", NULL, 0, NULL);
	//Sleep(3000);
	////恢复
	//mciSendString(L"resume LBI利比-小城夏天.mp3", NULL, 0, NULL);
	////关闭
	//Sleep(3000);
	//mciSendString(L"close LBI利比-小城夏天.mp3", NULL, 0, NULL);


	//注意：
	//1、音乐名不能有空格
	//2、网易云音乐 不能播放，QQ音乐的
	//3、不要擅自修改后缀名


	//6.文字
	settextcolor(BLACK);
	settextstyle(24, 0, L"微软雅黑");
	outtextxy(200, 100, L"我喜欢你");

	//7.图片
	//批量绘图
	BeginBatchDraw();

	IMAGE bkimg;// 定义图片类型                                  //基本绘图
	loadimage(&bkimg, L"bk.jpeg", 1200, 800);//加载图片
	putimage(0, 0, &bkimg);

	setbkmode(0);//让背景透明
	settextcolor(BLACK);
	settextstyle(24, 0, L"微软雅黑");
	outtextxy(200, 100, L"我喜欢你");

	IMAGE bk2img;// 定义图片类型
	loadimage(&bk2img, L"bk2.jpeg");//加载图片
	putimage(200, 500, &bk2img);
	
	EndBatchDraw();
	while (1)
	{

	}

	return 0;
}