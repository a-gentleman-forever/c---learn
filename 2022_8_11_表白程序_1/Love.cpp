#define  _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <easyx.h>
#include <mmsystem.h>
#include <stdlib.h>
#include <time.h>
#pragma comment(lib, "winmm.lib")

int main()
{
	//创建窗口
	initgraph(800, 500);

	//加载背景图片
	IMAGE bkimg[8];
	for (int i = 0; i < 8; i++)
	{
		char path[20];
		sprintf(path, "%d.png", i + 1);
		loadimage(&bkimg[i], path, 800, 500);
	}
	//加载苹果
	IMAGE appimg0, appimg1;
	loadimage(&appimg0, "apple0.jpg");
	loadimage(&appimg1, "apple1.jpg");
	//加载音乐
	mciSendString("open 告白气球.mp3", 0, 0, 0);
	mciSendString("play 告白气球.mp3 repeat", 0, 0, 0);

	//文字背景透明
	setbkmode(0);

	int y = 0, x = 0; //苹果的坐标
	char ch;  //下落的字母 
	int score = 0; //分数
	int index = 0; //索引
	int speed = 10;// 速度控制 25 分加速

	srand((unsigned int)time(NULL));

	while (1)
	{
		x = rand() % 750;
		ch = 'A' + rand() % 26;
		for (y = 0; y <= 500; y++)
		{
			BeginBatchDraw();
			//输出图片	
			putimage(0, 0, &bkimg[index]);
			putimage(x, y, &appimg0, SRCPAINT);
			putimage(x, y ,&appimg1, SRCAND); //C语言里面的与运算 :&

			outtextxy(x + 20, y + 20, ch);
			//输出成绩
			char str[20];
			sprintf(str, "分数：%d", score);
			outtextxy(5, 5, str);

			//接

			ExMessage m; //消息
			peekmessage(&m, EM_KEY);
			if (m.message == WM_KEYDOWN)
			{
				if (m.vkcode == ch)
				{
					score += 5;
					if (speed >= 4 && score % 25 == 0)
					{
						speed -= 1;
					}
					index++;
					if (index == 8)
					{
						index = 0;
					}
					break;
				}
			}

			EndBatchDraw();

			Sleep(speed);
		}
		
	}

	return 0;
}