#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
using namespace std;

class Date
{
public:
	// 获取某年某月的天数
	int GetMonthDay(int year, int month)
	{
		int array[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
		if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0))
		{
			return 29;
		}
		else
		{
			return array[month];
		}
	}

	// 全缺省的构造函数
	Date(int year = 1900, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	// 拷贝构造函数
  // d2(d1)
	Date(const Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}

	// 赋值运算符重载
  // d2 = d3 -> d2.operator=(&d2, d3)
	Date& operator=(const Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}

	// 析构函数
	~Date()
	{

	}

	// 日期+=天数
	Date& operator+=(int day)
	{
		_day += day;
		while (_day > GetMonthDay(_year, _month))
		{
			_day -= GetMonthDay(_year, _month);
			_month++;
			if (_month > 12)
			{
				_year++;
				_month = 1;
			}
		}

		return *this;
	}

	// 日期+天数
	Date operator+(int day)
	{
		Date d(*this);

		d._day += day;
		while (d._day > GetMonthDay(d._year, d._month))
		{
			d._day -= GetMonthDay(d._year, d._month);
			d._month++;
			if (d._month > 12)
			{
				d._year++;
				d._month = 1;
			}
		}

		return d;
	}

	// 日期-天数
	Date operator-(int day)
	{
		Date d(*this);

		d._day -= day;
		while (d._day <= 0)
		{
			d._day += GetMonthDay(d._year, d._month);
			d._month--;
			if (d._month <= 0)
			{
				d._year--;
				d._month = 12;
			}
		}

		return d;
	}

	// 日期-=天数
	Date& operator-=(int day)
	{
		_day -= day;
		while (_day <= 0)
		{
			_day += GetMonthDay(_year, _month);
			_month++;
			if (_month <= 0)
			{
				_year--;
				_month = 12;
			}
		}

		return *this;
	}

	// 前置++
	Date& operator++()
	{
		_day++;
		if (_day > GetMonthDay(_year, _month))
		{
			_day -= GetMonthDay(_year, _month);
			_month++;
			if (_month > 12)
			{
				_year++;
				_month = 1;
			}
		}

		return *this;
	}

	// 后置++
	Date operator++(int)
	{
		Date d(*this);

		_day++;
		if (_day > GetMonthDay(_year, _month))
		{
			_day -= GetMonthDay(_year, _month);
			_month++;
			if (_month > 12)
			{
				_year++;
				_month = 1;
			}
		}

		return d;
	}

	// 后置--
	Date operator--(int)
	{
		Date d(*this);

		_day--;
		if (_day <= 0)
		{
			_day += GetMonthDay(_year, _month);
			_month--;
			if (_month <= 0)
			{
				_year--;
				_month = 12;
			}
		}

		return d;
	}

	// 前置--
	Date& operator--()
	{
		_day--;
		if (_day <= 0)
		{
			_day += GetMonthDay(_year, _month);
			_month--;
			if (_month <= 0)
			{
				_year--;
				_month = 12;
			}
		}

		return *this;
	}

	// >运算符重载
	bool operator>(const Date& d)
	{
		if (_year > d._year)
		{
			return true;
		}
		else if (_year < d._year)
		{
			return false;
		}
		else
		{
			if (_month > d._month)
			{
				return true;
			}
			else if (_month < d._month)
			{
				return false;
			}
			else
			{
				if (_day > d._day)
				{
					return true;
				}
				else if (_day < d._day)
				{
					return false;
				}
				else
				{
					return false;
				}
			}
		}
	}

	// ==运算符重载
	bool operator==(const Date& d)
	{
		if (_year == d._year && _month == d._month && _day == d._day)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// >=运算符重载
	bool operator >= (const Date& d)
	{
		if (_year > d._year)
		{
			return true;
		}
		else if (_year < d._year)
		{
			return false;
		}
		else
		{
			if (_month > d._month)
			{
				return true;
			}
			else if (_month < d._month)
			{
				return false;
			}
			else
			{
				if (_day > d._day)
				{
					return true;
				}
				else if (_day < d._day)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
		}
	}

	// <运算符重载
	bool operator < (const Date& d)
	{
		{
			if (_year < d._year)
			{
				return true;
			}
			else if (_year > d._year)
			{
				return false;
			}
			else
			{
				if (_month < d._month)
				{
					return true;
				}
				else if (_month > d._month)
				{
					return false;
				}
				else
				{
					if (_day < d._day)
					{
						return true;
					}
					else if (_day > d._day)
					{
						return false;
					}
					else
					{
						return false;
					}
				}
			}
		}
	}

	// <=运算符重载
	bool operator <= (const Date& d)
	{
		{
			if (_year < d._year)
			{
				return true;
			}
			else if (_year > d._year)
			{
				return false;
			}
			else
			{
				if (_month < d._month)
				{
					return true;
				}
				else if (_month > d._month)
				{
					return false;
				}
				else
				{
					if (_day < d._day)
					{
						return true;
					}
					else if (_day > d._day)
					{
						return false;
					}
					else
					{
						return true;
					}
				}
			}
		}
	}

	// !=运算符重载
	bool operator != (const Date& d)
	{
		if (_year != d._year)
		{
			return true;
		}
		else if (_month != d._month)
		{
			return true;
		}
		else if (_day != d._day)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	// 日期-日期 返回天数
	int operator-(const Date& d)
	{
		Date d1(*this);
		Date d2(d);

		int num = 0;
		if (d1 >= d2)
		{
			while (d2 != d1)
			{
				num++;
				d2._day++;
				if (d2._day > GetMonthDay(d2._year, d2._month))
				{
					d2._day = 1;
					d2._month++;

					if (d2._month > 12)
					{
						d2._year++;
						d2._month = 1;
					}
				}
			}
		}
		else 
		{
			while (d2 != d1)
			{
				num--;
				d1._day++;
				if (d1._day > GetMonthDay(d1._year, d1._month))
				{
					d1._day = 1;
					d1._month++;

					if (d1._month > 12)
					{
						d1._year++;
						d1._month = 1;
					}
				}
			}
		}

		return num;
	}

private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1(2022,7,1);
	Date d2(2023,7,31);

	int num = d1 - d2;
	int num1 = d2 - d1;

	cout << num << endl;
	cout << num1 << endl;

	return 0;
}