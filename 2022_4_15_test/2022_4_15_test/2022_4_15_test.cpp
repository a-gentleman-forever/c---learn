#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;
////成员属性设为私有
//class Person
//{
//public:
//	void SetName(string name)
//	{
//		m_Name = name;
//	}
//	// 获取姓名
//	string GetName()
//	{
//		return m_Name;
//	}
//
//	// 设置年龄
//	void SetAge(int age)
//	{
//		if (age >= 0 && age <= 150)
//		{
//			m_Age = age;	
//		}
//		else
//		{
//			m_Age = 0;
//			cout << "老妖精" << endl;
//		}
//	}
//
//	// 获取年龄
//	int GetAge()
//	{
//		return m_Age;
//	}
//	 
//	// 设置女朋友
//	void SetLover(string name)
//	{
//		m_Lover = name;
//	}
//
//private:
//	string m_Name;
//
//	int m_Age;
//
//	string m_Lover;
//};
//
//int main()
//{
//	Person p1;
//	p1.SetName("张三");
//	p1.SetLover("小王");
//	p1.SetAge(1000);
//	cout << "姓名： " << p1.GetName() << endl;
//	cout << "年龄： " << p1.GetAge() << endl;
//
//	
//
//	system("pause");
//
//	return 0;
//}
//
//class Cube
//{
//public:
//	void SetH(int h)
//	{
//		m_H = h;
//	}
//	void SetW(int w)
//	{
//		m_W = w;
//	}
//	void SetL(int l)
//	{
//		m_L = l;
//	}
//
//	int GetL()
//	{
//		return m_L;
//	}
//	int GetH()
//	{
//		return m_H;
//	}
//	int GetW()
//	{
//		return m_W;
//	}
//
//	// 得到计算面积
//	int GetS()
//	{
//		return (m_H * m_W) * 2 + (m_H * m_L) * 2 + (m_W * m_L);
//	}
//
//	// 得到计算体积
//	int GetV()
//	{
//		return m_H * m_W * m_L;
//	}
//
//	// 判断VS是否相等
//	bool JudSAB()
//	{
//		if (GetV() == GetS())
//		{
//			return true;
//		}
//		else
//		{
//			return false;
//		}
//	}
//
//	// 判断和其他长方体是否相等
//	bool JudCube(Cube &c)
//	{
//		if (m_L == c.GetL() && m_H == c.GetH() && m_W == c.GetW())
//		{
//			return true;
//		}
//		else
//		{
//			false;
//		}
//	}
//
//private:
//	int m_H;
//	int m_W;
//	int m_L;
//};
//
//int main()
//{
//	Cube c1;
//	Cube c2;
//
//	c1.SetH(10);
//	c1.SetL(10);
//	c1.SetW(10);
//
//	c2.SetH(10);
//	c2.SetL(10);
//	c2.SetW(10);
//
//	cout << "面积：" << c1.GetS() << endl;
//	cout << "体积：" << c1.GetV() << endl;
//
//	if (c1.JudCube(c2) == true)
//	{
//		cout << "体积和面积相等" << endl;
//	}
//	else
//	{
//		cout << "体积和面积不相等" << endl;
//	}
//
//	system("pause");
//
//	return 0;
//}

class Point
{
public:
	// 设置x坐标
	void SetX(int x)
	{
		m_X = x;
	}
	// 设置y坐标
	void SetY(int y)
	{
		m_Y = y;
	}
	// 获取x坐标
	int GetX()
	{
		return m_X;
	}
	// 获取y坐标
	int GetY()
	{
		return m_Y;
	}

private:

	int m_X;

	int m_Y;
};

class Circle
{
public:
	// 设置R
	void SetR(int r)
	{
		m_R = r;
	}
	//获取R
	int GetR()
	{
		return m_R;
	}
	// 设置圆心
	void SetCenter(Point center)
	{
		m_Center = center;
	}
	// 获取圆心
	Point GetCenter()
	{
		return m_Center;
	}
	// 判断点和圆的关系
	void IsInCircle(Point &p)
	{
		int distance =
			(m_Center.GetX() - p.GetX()) * (m_Center.GetX() - p.GetX()) +
			(m_Center.GetY() - p.GetY()) * (m_Center.GetY() - p.GetY());

		if (distance == m_R * m_R)
		{
			cout << "点在圆上" << endl;
		}
		else if (distance < m_R * m_R)
		{
			cout << "点在圆内" << endl;
		}
		else
		{
			cout << "点在圆外" << endl;
		}
	}

private:

	int m_R;

	Point m_Center;
};

int main()
{
	// 创建圆
	Circle c1;
	Point ctr1;
	
	ctr1.SetX(10);
	ctr1.SetY(0);

	c1.SetR(10);
	c1.SetCenter(ctr1);
	// 创建点
	Point p1;

	p1.SetX(10);
	p1.SetY(11);
	// 判断
	cout << "圆心：" << c1.GetCenter().GetX() << "," << c1.GetCenter().GetY() << endl;
	cout << "半径：" << c1.GetR() << endl;
	cout << "点：" << p1.GetX() << "," << p1.GetY() << endl;
	c1.IsInCircle(p1);

	system("pause");

	return 0;
}