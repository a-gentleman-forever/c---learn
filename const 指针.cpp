#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

int main()
{
	int a = 10;
	int b = 20;

	const int* p = &a;
	p =  &b;

	cout << *p << endl;
	cout << b << endl;

	system("pause");

	return 0;
}