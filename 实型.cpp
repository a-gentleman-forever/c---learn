#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

//float
//double	
int main()
{
	float f = 3.1415926f;
	double lf = 3.1415926;

	cout << f << endl;
	cout << lf << endl;

	//科学计数法
	float f2 = 3e2;

	cout << f2 << endl;

	system("pause");

	return 0;
}