#pragma once

#include <iostream>
#include <assert.h>
using namespace std;

namespace mr
{
	template<class T>
	class vector
	{
	public:
		// iterator
		typedef T* iterator;
		typedef const T* const_iterator;
		iterator begin()
		{
			return  _start;
		}
		iterator end()
		{
			return _finish;
		}
		const_iterator cbegin() const
		{
			return _start;
		}
		const_iterator cend() const
		{
			return _finish;
		}
		// construct and destroy
		vector()
			:_start(nullptr)
			, _finish(nullptr)
			, _endOfstorage(nullptr)
		{

		}
		vector(int n, const T& value = T())
			:_start(new T[n](T))
			,_finish(_start + n)
			,_endOfstorage(_finish)
		{
			
		}
		template<class Inputlterator>
		vector(Inputlterator first, Inputlterator last)
		{

		}
		vector(const vector<T>& v)
		{
			*this = v;
		}
		vector<T>& operator=(vector<T> v)
		{
			size_t tep = _finish - _start;
			vector<T> tmp = new T[_finish - _start + 1];
			for (auto e : _start)
			{
				*tmp++ = e;
			}
			delete[] _start;

			_start = tmp;
			_finish = tmp + tep;
			_endOfstorage = _start + tep;

			return *this;
		}
		~vector()
		{
			delete[] _start;
			_start = _finish = _endOfstorage = nullptr;
		}

		////capacity
		size_t size() const
		{
			return _finish - _start;
		}
		size_t capacity() const
		{
			return _endOfstorage - _start;
		}
		void reserve(size_t n)
		{
			if (capacity() < n)
			{
				size_t sz = size();
				T* tmp = new T[n];	

				memcpy(tmp, _start, sizeof(T) * size());
				delete[] _start;

				_start = tmp;
				_finish = _start + sz;
				_endOfstorage = _start + n;
			}
			else
			{
				_endOfstorage = _start + n;
			}
		}
		void resize(size_t n, const T& value = T())
		{
			assert(n);

			if (capacity() < n)
			{
				reserve(n);
			}

			vector<T>::iterator it = begin();
			while (it + size() < _start + n)
			{
				*(it + size()) = value;
				++it;
			}

			_finish = _start + n;
		}

		//// access 
		T& operator[](size_t pos)
		{
			return _start[pos];
		}
		const T& operator[](size_t pos) const
		{
			return _start[pos];
		}

		////modify
		void push_back(const T& x)
		{
			if (capacity() == size())
			{
				reserve(capacity() == 0 ? 4 : capacity() * 2);
			}

			_start[size()] = x;
			_finish++;
		}
		void pop_back()
		{
			assert(_start);

			if (_finish == _start)
			{
			return;
			}

			_finish--;

		}
		void swap(vector<T>& v)
		{
			vector<T> tmp = v;
			v = *this;
			*this = tmp;
		}
		iterator insert(iterator pos, const T& x)
		{
			if (capacity() == size())
			{
				reserve(capacity() == 0 ? 4 : capacity() * 2);
			}

			vector<T>::iterator end = end();
			while (end > _start + pos)
			{
				*end = *(end - 1);
				--end;
			}

			*(_start + pos) = x;
			_finish++;

			return _start + pos;
		}
		iterator erase(iterator pos)
		{
			vector<T>::iterator cur = _start + pos;

			while (cur < end())
			{
				*cur = *(cur + 1);
				++cur;
			}

			_finish--;

			return _start + pos;
		}
	private:
		iterator _start;
		iterator _finish;
		iterator _endOfstorage;
	};

	void test_1()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(5);

		v1.pop_back();

		v1.resize(2);

		vector<int>::iterator it = v1.begin();
		while (it < v1.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;


		v1.reserve(100);
		v1.resize(2000);
		cout << "capacity::" << v1.capacity() << endl;
		cout << "size::" << v1.size() << endl;
	}
}

