#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

// >
// <
// >=
// <=
// ==
// !=
// 如果成立则是 1
// 如果不成立则是 0
int main()
{
	int a = 10;
	int b = 20;

	cout << (a > b) << endl;

	system("pause");

	return 0;
}
