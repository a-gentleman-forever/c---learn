#pragma once

#include "Common.h"

template<class T>
class ObjectPool
{
public: 
	T* New()
	{
		T* obj = (T*)_memory;

		// 优先把换回来的内存块对象，再次重复使用
		if (_freeList)
		{
			void* next = *((void**)_freeList);
			obj = (T*)_freeList;
			_freeList = next;

			return obj; 
		}
		else
		{
			// 剩余内存不够一个对象大小时，则重新开辟大块空间
			if (_remainBytes < sizeof(T))
			{ 
				_remainBytes = 128 * 1024;
				// _memory = (char*)malloc(128 * 1024);
				_memory = (char*)SystemAlloc(_remainBytes >> 13);
				if (_memory == nullptr)
				{
					throw bad_alloc();
				}
			}  

			obj = (T*)_memory;
			size_t objSize = sizeof(T) < sizeof(void*) ? sizeof(void*) : sizeof(T);
			_memory += objSize;
			_remainBytes -= objSize;
		}

		// 定位new, 显示调用T的构造函数初始化
		new(obj)T;

		return obj;
	}

	void Delete(T* obj)
	{
		// 显示调用析构函数清理对象 
		obj->~T();

		if (_freeList == nullptr)
		{
			_freeList = obj;
			*(void**)obj = nullptr; // 把内存的前四个字节存放为空, 用来存放下一个内存块的基地址 
		}
		else
		{
			// 头插
			*(void**)obj = _freeList;
			_freeList = obj;
		}
	}
private:

	char* _memory = nullptr; // 指向大块内存的指针
	size_t _remainBytes = 0; // 大块内存在切分过程中剩余字节数
	void* _freeList = nullptr; // 换回来过程中链接的自由链表头指针
};