#pragma once
#pragma warning(disable:26814)
#pragma warning(disable:26440)
#pragma warning(disable:26493)
#pragma warning(disable:26409)
#pragma warning(disable:26496)
#pragma warning(disable:26482)
#pragma warning(disable:26446)
#pragma warning(disable:26451)
#pragma warning(disable:6297)
#pragma warning(disable:26497)
#pragma warning(disable:26447)
#pragma warning(disable:26461)
#pragma warning(disable:26462)
#pragma warning(disable:26455)
#pragma warning(disable:26481)
#pragma warning(disable:26400)
#pragma warning(disable:26402)
#pragma warning(disable:6246)
#pragma warning(disable:26110)
#pragma warning(disable:26401)
#pragma warning(disable:4839)

#include <iostream>
#include <assert.h>
#include <thread>
#include <mutex>
#include <time.h>
#include <vector>
#include <unordered_map>
#include <algorithm>

#ifdef _WIN32
#include <windows.h>
#else
	// linux...
#endif

using std::cout;
using std::endl;
using std::bad_alloc;

static const size_t MAX_BYTES = 256 * 1024;
static const size_t NFREE_LIST = 208;
static const size_t NPAGES = 129;
static const size_t PAGE_SHIFT = 13;

#ifdef _WIN64
	typedef unsigned long long PAGE_ID;
#elif _WIN32
	typedef size_t PAGE_ID;
#else
	// linux
#endif // __WIN32

inline static void* SystemAlloc(size_t kpage)
{
#ifdef _WIN32
	void* ptr = VirtualAlloc(0, kpage << 13, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
#else
	// linux下brk mmap等
#endif

	if (ptr == nullptr)
	{
		throw std::bad_alloc();
	}

	return ptr;
}


inline static void SystemFree(void* ptr)
{
#ifdef _WIN32
	VirtualFree(ptr, 0, MEM_RELEASE);
#else
	// linux下brk mmap等
#endif
}


static void*& NextObj(void* obj)
{
	// 取头上4/8字节
	return *(void**)obj;
}

// 管理切分好的小对象的自由链表
class FreeList
{
public:
	void Push(void* obj)
	{
		// 头插
		//*(void**)obj = _freeList;
		NextObj(obj) = _freeList;
		_freeList = obj;

		++_size;
	}

	void PushRange(void* start, void* end, size_t n)
	{
		NextObj(end) = _freeList;
		_freeList = start;

		_size += n;
	}

	void PopRange(void*& start, void*& end, size_t n)
	{	
		// 提供start - end 的 n 个对象的内存
		assert(n >= _size);

		start = _freeList;
		end = start;

		for (size_t i = 0; i < n - 1; ++i)
		{
			end = NextObj(end);
		}

		// 调试代码
		// 这里疑似死循环 使用中断程序，程序会在运行的地方停下来 
		//void* cur = _freeList;
		//int i = 0;
		//while (cur)
		//{
		//	cur = NextObj(cur);
		//	++i;
		//}

		//if (i != n)
		//{
		//	int a = 0;
		//}

		_freeList = NextObj(end);
		NextObj(end) = nullptr;
		_size -= n;
	}

	void* Pop()
	{
		assert(_freeList);
		// 头删
		void* obj = _freeList;
		_freeList = NextObj(obj);

		--_size;

		return obj;
	}

	bool Empty()
	{
		return _freeList == nullptr; 
	}

	size_t& MaxSize()
	{
		return _maxSize;
	}

	size_t Size()
	{
		return _size;
	}

private:
	void* _freeList =  nullptr;
	size_t _maxSize = 1; // 慢增长调节的算法使用的参数
	size_t _size = 0; // 自由链表中可以使用的节点
};

// 计算对象大小的对齐映射规则
class SizeClass
{
public:
	// 整体控制在最多10%左右的内碎片浪费
	// [1,128]				8byte对齐			freelist[0,16)
	// [128+1,1024]			16byte对齐			freelist[16,72)
	// [1024+1,8*1024]		128byte对齐			freelist[72,128)
	// [8*1024+1,64*1024]	1024byte对齐			freelist[128,184)
	// [64*1024+1,256*1024] 8*1024byte对齐		freelist[184,208)
	/*size_t _RoundUp(size_t size, size_t alignNum)
	{
		size_t  alignSize = size;
		if (size % 8 != 0)
		{
			alignSize = (size / alignNum + 1)*alignNum;
		}
		else
		{
			alignSize = size;
		}

		return alignSize;
	}*/
	static inline size_t _RoundUp(size_t bytes, size_t align)
	{
		return (((bytes) + align - 1) & ~(align - 1));
	}

	static size_t RoundUp(size_t size)
	{
		if (size <= 128)
		{
			return _RoundUp(size, 8);
		}
		else if (size <= 1024)
		{
			return _RoundUp(size, 16);
		}
		else if (size <= 8*1024)
		{
			return _RoundUp(size, 8 * 1024); 
		}
		else if (size <= 64*1024)
		{
			return _RoundUp(size, 16);
		}
		else if (size <= 256*1024)
		{
			return _RoundUp(size, 256 * 1024);
		}
		else
		{
			return _RoundUp(size, 1 << PAGE_SHIFT);
		}
	}

	/*size_t _Index(size_t bytes, size_t alignNum) 
	{
		if (bytes % alignNum == 0)
		{
			return bytes / alignNum - 1;
		}
		else
		{
			return bytes / alignNum;
		}
	}*/

	static inline size_t _Index(size_t bytes, size_t align_shift)
	{
		return ((bytes + (1 << align_shift) - 1) >> align_shift) - 1;
	}

	// 计算映射的哪一个自由链表桶
	static inline size_t Index(size_t bytes)
	{
		assert(bytes <= MAX_BYTES);

		// 每个区间多少个链
		static int group_array[4] = { 16, 56, 56, 56 };
		if (bytes <= 128)
		{
 			return _Index(bytes, 3);
		}
		else if (bytes <= 1024)
		{
			return _Index(bytes - 128, 4) + group_array[0];
		}
		else if (bytes <= 8*1024)
		{
			return _Index(bytes - 1024, 7) + group_array[0] + group_array[1];
		}
		else if (bytes <= 64*1024)
		{
			return _Index(bytes - 8*1024, 7) + group_array[0] + group_array[1] +  group_array[2];
		}
		else if (bytes <= 256*1024)
		{
			return _Index(bytes - 1024, 7) + group_array[0] + group_array[1] + group_array[2] + group_array[3];
		}
		else
		{
			assert(false);
			return -1;
		}
	}

	// 一次thread Cache从中心缓存拿多少个
	static size_t NumMoveSize(size_t size)
	{
		assert(size > 0);

		if (size == 0)
		{
			return 0;
		}

		int	num = MAX_BYTES / size;
		if (num < 2)
		{
			num = 2;
		}

		if (num > 512)
		{
			num = 512;
		}

		return num;
	}

	// 计算一次向系统获取几个页
	// 单个对象 8byte
	// 单个对象
	static size_t NumMovePage(size_t size)
	{
		size_t num = NumMoveSize(size);
		size_t npage = num * size;

		npage >>= PAGE_SHIFT;
		if (npage == 0)
		{
			npage = 1;
		}

		return npage;
	}
};

// 管理多个连续页大块内存跨度结构
struct Span
{
	PAGE_ID _pageId = 0; // 大块内存的起始页的页号
	size_t _n = 0; // 页的数量

	Span* _next = nullptr; // 双向链表结构
	Span* _prev = nullptr;

	size_t _objSize = 0; // 切好的小对象的大小

	size_t _useCount = 0; // 切好的小块内存被分配给ThreadCache的计数
	void* _freeList = nullptr; // 切好的小块内存的自由链表

	bool _isUse = false;			// 是否在被使用
};

// 带头双向循环链表
class SpanList
{
public:
	SpanList()
	{
		_head = new Span;
		_head->_next = _head;
		_head->_prev = _head;
	}

	void Insert(Span* pos, Span* newSpan)
	{
		assert(pos);
		assert(newSpan);

		Span* prev = pos->_prev;
		prev->_next = newSpan;
		newSpan->_prev = prev;
		newSpan->_next = pos;
		pos->_prev = newSpan;
	}

	Span* Begin()
	{
		return _head->_next;
	}

	Span* End()
	{
		return _head;
	}

	bool Empty()
	{
		return _head->_next == _head;
	}

	void PushFront(Span* span)
	{
		Insert(Begin(), span);
	}

	Span* PopFront()
	{
		Span* front = _head->_next;
		Erase(front);

		return front;
	}

	void Erase(Span* pos)
	{
		assert(pos);
		assert(pos != _head);
		// 调试技巧
		// 1、条件断点
		// 2、查看栈帧
		//if (pos == _head)
		//{
		//	int x = 0;
		//}

		Span* prev = pos->_prev;
		Span* next = pos->_next;

		prev->_next = next; 
		next->_prev = prev;
	}

private:
	Span* _head;

public:
	std::mutex _mtx; // 桶锁
};