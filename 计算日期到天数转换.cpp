#define  _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

class Date
{
    friend istream& operator>>(istream& in, Date& d);
public:
    Date(int year = 1990, int month = 1, int day = 1)
    {
        _year = year;
        _month = month;
        _day = day;
    }

    int GetMonthDays(int year, int month)
    {
        int days[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
        int day = days[month];

        if (month == 2
            && ((year % 4 == 0 && year % 100 != 0)
                || (year % 400 == 0)))
        {
            day = 29;
        }

        return day;
    }
    //计算今年第几天
    int CountDays()
    {
        int days = 0;

        for (int i = 0; i < _month; i++)
        {
            days += GetMonthDays(_year, i);
        }
        days += _day;

        return days;
    }

private:
    int _year;
    int _month;
    int _day;
};

istream& operator>>(istream& in, Date& d)
{
    in >> d._year >> d._month >> d._day;

    return in;
}

int main() {
    Date d;

    cin >> d;

    cout << d.CountDays() << endl;

    return 0;
}