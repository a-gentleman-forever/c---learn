#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

int main()
{
	int arr[5] = { 1,2,3,4,5 };

	int left = 0;
	int right = 4;
	for (int i = 0; i < 5/2; i++)
	{
		int tmp = arr[left];
		arr[left] = arr[right];
		arr[right] = tmp;

		left++;
		right--;
	}

	for (int i = 0; i < 5; i++)
	{
		cout << arr[i] << endl;
	}

	system("pause");

	return 0;
}