#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

int main()
{
	int a = 10;
	int& b = a;
	int& c = a;

	cout << a << endl;

	b = 20;
	cout << a << endl;

	c = 30;	
	cout << a << endl;

	system("pause");

	return 0;
}