#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

//int func(int a, int b = 20, int c = 30)
//{
//	return a + b + c;
//}

void func(int a, int)
{
	cout << "this is func" << endl;
}

void func1(int a, int = 10)
{
	cout << "this is func" << endl;
}


int main()
{
	func(10, 10);

	system("pause");

	return 0;
}