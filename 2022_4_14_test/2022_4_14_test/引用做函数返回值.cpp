#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

int& test1()
{
	int a = 10;

	return a;
}

int& test2()
{
	static int a = 10;

	return a;
}

int& test3()
{
	static int a = 10;

	return a;
}

int main()
{
	int& ref = test1();
	int& ref2 = test2();

	//cout << ref << endl;
	//cout << ref << endl;
	//cout << ref << endl;

	//cout << ref2 << endl;
	//cout << ref2 << endl;
	//cout << ref2 << endl;
	//cout << ref2 << endl;

	//int& ref3 = test3();
	test3() = 1000;

	cout << test3() << endl;

	system("pause ");

	return 0;
}

//int main()
//{
//	int a = 10;
//	int& b = a;
//
//	cout << b << endl;
//
//	return 0;
//}

//void Swap(int &a, int &b)
//{
//	int tmp = a;
//	a = b;
//	b = tmp;
//}
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//
//	Swap(a, b);
//	cout << "a:" << a << endl;
//	cout << "a" << b << endl;
//
//	system("pause");
//
//	return 0;
//}