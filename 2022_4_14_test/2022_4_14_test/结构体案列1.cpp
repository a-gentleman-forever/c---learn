#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

struct S
{
	string name;
};

int main()
{
	S s1;

	s1.name = 'M';
	cout << s1.name << endl;

	s1.name += 'R';
	cout << s1.name << endl;

	system("pause");

	return 0;
}