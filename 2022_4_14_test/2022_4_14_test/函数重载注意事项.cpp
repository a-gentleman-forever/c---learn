#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

void func(int& a)
{
	cout << "func(int &a)调用" << endl;
}

void func(const int &a)
{
	cout << "func(const int &a)调用" << endl;
}

void func2(int a, int b = 10)
{
	cout << "func(int a)" << endl;
}

void func2(int a)
{
	cout << "func(a)" << endl;
}


int main()
{
	int a = 0;
	//func(a);
	//func(10);

	//func2(a);二义性
	func2(a, 10);

	system("pause");

	return 0;
}