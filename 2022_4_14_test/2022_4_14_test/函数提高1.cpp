#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

int func(int a, int b, int)
{
	return a + b;
}

int func(int a, int b)
{
	return a - b;
}

int func(double a, double b)
{
	return a - b;
}


int main()
{
	cout << func(10, 20, 1) << endl;
	cout << func(10, 20) << endl;
		
	system("pause");

	return 0;
}