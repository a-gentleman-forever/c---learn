#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

int main()
{
	int* a = new int[10];

	int* m = a;

	for (int i = 0; i < 10; i++)
	{
		cout << a[i] << endl;
	}

	delete[] m;

	system("pause");

	return 0;
}