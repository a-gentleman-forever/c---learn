#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

int* Func()
{
	int* a = new int(10);

	return a;
}

int main()
{
	int* p = Func();

	cout << *p << endl;
	cout << *p << endl;

	system("pause");

	return 0;
}