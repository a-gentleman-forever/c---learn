#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

//const double PI = 3.14;
//// 设计一个圆类，计算圆周长
//class Circle
//{
//	// 访问权限
//public:
//	// 属性
//	int m_r;
//	// 行为
//	double calculateZC()
//	{
//		return 2 * PI * m_r;
//	}
//};
//
//int main()
//{
//	// 通过圆类创建具体的圆，这个圆就是对象
//	Circle c1;
//	
//	c1.m_r = 10;
//	cout << "圆的周长：" << c1.calculateZC() << endl;
//
//	system("pause");
//
//	return 0;
//}

//class Student
//{
//public: // 公共权限
//	// 属性
//	string m_name;
//	string m_string;
//	// 行为
//
//	void SetName(string name)
//	{
//		m_name = name;
//	}
//	void SetString(string str)
//	{
//		m_string = str;
//	}
//	void Print()
//	{
//		cout << m_name << endl;
//		cout << m_string << endl;
//	}
//};
//
//int main()
//{
//	Student s1;
//
//	s1.m_name = "刘曾铭钰";
//	s1.m_string = "123456789";
//
//	s1.Print();
//
//	Student s2;
//
//	s2.SetName("maorui");
//	s2.SetString("23456");
//	s2.Print();
//
//	system("pause");
//
//	return 0;
//}

class Person
{
	// 公共
public:
	string m_Name;

	// 保护
protected:
	string m_Car;

	//私有
private:
	string m_Password;

public:
	void Set()
	{
		m_Name = "张三";
		m_Car = "拖拉机";
		m_Password = "12345";
	}
};

int main()
{
	Person p1;

	p1.m_Name = "hehe";
	// p1.m_Car = "da"; // 类外不可访问

	system("pause");

	return 0;
}