#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <string>
#include <functional>
#include <map>
#include <set>
using namespace std;

void test_set()
{
	//set<int> s;
	//s.insert(4);
	//s.insert(2);
	//s.insert(8);
	//s.insert(1);
	//s.insert(5);
	//s.insert(6);

	set<int, greater<int>>  s = { 14,2,1,8,5,6 };

	for (auto& e : s)
	{
		cout << e << " ";
	}
	cout << endl;

	s.erase(2); //如果存在数值就删除，不存在不报错
	for (auto& e : s)
	{
		cout << e << " ";
	}
	cout << endl;

	set<int>::iterator pos = s.find(5);//不存在报错
	s.erase(pos);
	for (auto& e : s)
	{
		cout << e << " ";
	}
	cout << endl;

	//count检查是否有某个数据 bool类型
	cout << s.count(8) << endl;
	cout << s.count(30) << endl;
}

void test_set2()
{
	std::set<int> myset;
	std::set<int>::iterator itlow, itup;

	for (int i = 1; i < 10; i++) myset.insert(i * 10); // 10 20 30 40 50 60 70 80 90

	itlow = myset.lower_bound(35);                //
	itup = myset.upper_bound(60);                 //
	cout << "itlow:" << *itlow << endl;
	cout << "itup:" << *itup << endl;

	myset.erase(itlow, itup);                     // 10 20 70 80 90

	std::cout << "myset contains:";
	for (std::set<int>::iterator it = myset.begin(); it != myset.end(); ++it)
	std::cout << ' ' << *it;
	std::cout << '\n';
}

void test_set3()
{
	multiset<int> s = { 1,2,1,3,5,8,10,3,3 };

	for (auto& e : s)
	{
		cout << e << " ";
	}
	cout << endl;

	cout << s.count(1) << endl;

	//find如果有多个值，返回中序的第一个值
	auto pos = s.find(3);
	while (pos != s.end())
	{
		cout << *pos << " ";
		++pos;
	}
	cout << endl;

	s.erase(3);
	for (auto& e : s)
	{
		cout << e << " ";
	}
	cout << endl;

	pos = s.find(1);
	s.erase(pos);
	for (auto& e : s)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_map1()
{
	map<string, string> dict;
	//pair<string, string> kv1("sort", "排序");
	//dict.insert(kv1);

	dict.insert(pair<string, string>("sort", "排序"));
	dict.insert(pair<string, string>("test", "测试"));
	dict.insert(pair<string, string>("string", "字符串"));

	typedef pair<string, string> DictKv;
	dict.insert(DictKv("home", "家"));
	dict.insert(DictKv("home", "守候"));

	dict.insert(make_pair("left", "左边"));

	map<string, string>::iterator it = dict.begin();
	while (it != dict.end())
	{
		//cout << (*it).first << ":" << (*it).second << endl;
		cout << it->first << ":" << it->second << endl;; //编译器会自动省略一个箭头
		++it;
	}
	cout << endl;

	for (auto& kv : dict)
	{
		cout << kv.first << ":" << kv.second << endl;
	}
	cout << endl;
}

int main()
{
	test_map1();

	return 0;
}