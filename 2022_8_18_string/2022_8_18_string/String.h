#pragma once
#include <iostream>
#include <string>
#include <assert.h>
using namespace std;

namespace bit
{
	class string
	{
		friend ostream& operator<<(ostream& out, string& str);
		friend istream& operator>>(istream& out, string& str);
	public:
		typedef char* iterator;
	public:
		string(const char* s = "")
		{
			_size = strlen(s);
			_capacity = _size;
			_str = new char[_capacity + 1];

			strcpy(_str, s);
		}
		string(const string& str)
		{
			_size = str._size;
			_capacity = str._capacity;
			_str = new char[_capacity + 1];

			strcpy(_str, str._str);
		}
		~string()
		{
			delete[] _str;
			_str = nullptr;
			_size = _capacity = 0;
		}
		void swap(string& str)
		{
			::swap(_size, str._size);
			::swap(_capacity, str._capacity);
			::swap(_str, str._str);
		}
		string& operator=(const string& str)
		{
			string tmp(str);
			swap(tmp);

			return *this;
		}
		string& operator=(const char* s)
		{
			string tmp(s);
			swap(tmp);

			return *this;
		}

		///////////////////////////////

		iterator begin()
		{
			return _str;
		}
		iterator end()
		{
			return _str + _size;
		}

		///////////////////////////////

		void clear()
		{
			_size = 0;
		}
		size_t size()
		{
			return _size;
		} const
		size_t capacity()
		{
			return _capacity;
		} const
		bool empty() const
		{
			return _size == 0;
		}
		const char* c_str()
		{
			return _str;
		} const
		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);

				delete[] _str;

				_str = tmp;

				_capacity = n;
			}
		}
		void resize(size_t n, char ch = '\0')
		{
			if (_size <= n)
			{
				_size = n;
			}
			else
			{
				while (_size < n)
				{
					_str[_size++] = ch;
				}
			}

			_str[n] = '\0';
		}
		void push_back(char ch)
		{
			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}

			_str[_size] = ch;
			_str[++_size] = '\0';
		}
		string& append(const string& str)
		{
			*this += str;

			return *this;
		}
		string& insert(size_t pos, char c)
		{
			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}

			int end = _size++;
			while (end > (int)pos)
			{
				_str[end] = _str[end - 1];
				--end;
			}
			_str[_size] = '\0';

			_str[pos] = c;

			return *this;
		}
		string& insert(size_t pos, const char* str)
		{
			assert(pos < _capacity);

			size_t len = strlen(str);

			if (_size + len > _capacity)
			{
				reserve(_size + len);
			}

			int end = _size - 1;
			while (end >= (int)pos)
			{
				_str[end + len] = _str[end];
				--end;
			}
			_size += len;
			_str[_size] = '\0';

			end++;
			while (*str)
			{
				_str[end++] = *str++;
			}

			return *this;
		}
		string& operator+=(char ch)
		{
			push_back(ch);

			return *this;
		}
		string& operator+=(const char* s)
		{
			size_t len = strlen(s);

			if (_size + len > _capacity)
			{
				reserve(_size + len);
			}

			strcpy(_str + _size, s);
			_size += len;

			return *this;
		}
		string& operator+=(const string& str)
		{
			*this += str._str;

			return *this;
		}

		////////////////////////////////
		//[]
		char& operator[](size_t index)
		{
			return _str[index];
		}
		const char& operator[](size_t index) const
		{
			return _str[index];
		}

		/////////////////////////////////

		string& erase(size_t pos, size_t len = -1)
		{
			assert(pos < _size&& pos >= 0);
			len = len > _size - pos ? _size - pos : len;

			size_t cur = pos;
			while (cur <= _size - len)
			{
				_str[cur] = _str[cur + len];
				++cur;
			}

			--_size;

			return *this;
		}

		////////////////////////////////

		bool operator==(const string& str) const
		{
			return strcmp(_str, str._str) == 0;
		}
		bool operator>(const string& str) const
		{
			return strcmp(_str, str._str) > 0;
		}
		bool operator>=(const string& str) const
		{
			return (*this == str) && (*this > str);
		}
		bool operator<(const string& str) const
		{
			return !(*this >= str);
		}
		bool operator<=(const string& str) const
		{
			return !(*this > str);
		}
		bool operator!=(const string& str) const
		{
			return !(*this == str);
		}

		/////////////////////////////////

		size_t find(char c, size_t pos = 0) const
		{
			assert(pos > 0 && pos < _size);

			int flag = 0;
			size_t index = pos;

			while (index < _size)
			{
				if (_str[index] == c)
				{
					flag = 1;
					break;
				}

				index++;
			}

			if (flag)
				return index;
			else
				return -1;
		}
		size_t find(const char *s, size_t pos = 0) const
		{
			size_t cur = pos;
			size_t prev = pos;
			const char* src = s;

			while (cur < _size)
			{
				if (_str[cur] == *src)
				{
					src++;
					cur++;
				}
				else
				{
					++prev;

					src = s;
					cur = prev;

				}


				if (*src == '\0')
				{
					break;
				}
			}

			if (*src == '\0')
				return prev;
			else
				return -1;
		}
	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	};

	ostream& operator<<(ostream& out, string& str)
	{
		out << str.c_str();

		return out;
	}
	istream& operator>>(istream& in, string& str)
	{
		char ch;
		in >> ch;

		str._size = 0;

		while (ch != ' ' && ch != '\n')
		{
			str.push_back(ch);
			ch = getchar();
			
		}

		return in;
	}


	void test1_String()
	{
		string s1("hello world");
		string s2("hehehehe");

		//s1 += 'a';
		//s1 += 'a';
		//s1 += 'x';

		//s1 += "���";

		//s1 = "��� ����";
		//s1 += s2;
		//s1.append(s2);

		//s1.insert(0, "hehe");
		cout << s1 << endl;


		//s1.insert(5, 'a');
		//s1.insert(5, 'a');

		//s1.erase(5, 1);
		//s1.erase(5);

		cout << s1 << endl;
		cout << s1.find("world") << endl;
		cout << s1.find("hello") << endl;

		//cout << s1 << endl;
		//cout << s1.c_str() << endl;
		//cout << s1.size() << endl;
		//cout << s1.capacity() << endl;
	}
}
