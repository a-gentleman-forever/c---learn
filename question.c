#define  _CRT_SECURE_NO_WARNINGS 1

#include "Queue.h"
#include "Stack.h"
typedef int StackDateType;

typedef struct Stack
{
	StackDateType* a;
	int capacity;
	int size;
}ST;
// 初始化
void StackInit(ST* ps)
{
	assert(ps);

	ps->a = (ST*)malloc(sizeof(StackDateType) * 4);
	if (ps->a == NULL)
	{
		exit(-1);
	}
	ps->capacity = 4;
	ps->size = 0;
}
// 销毁
void StackDestory(ST* ps)
{
	assert(ps);

	free(ps->a);
	ps->capacity = 0;
	ps->size = 0;
}
// 入栈
void StackPush(ST* ps, StackDateType x)
{
	assert(ps);
	// 检测
	if (ps->size == ps->capacity)
	{
		StackDateType* tmp = (ST*)realloc(ps->a, sizeof(StackDateType) * ps->capacity * 2);
		if (tmp == NULL)
		{
			exit(-1);
		}
		ps->a = tmp;

		ps->capacity *= 2;
	}

	ps->a[ps->size] = x;
	ps->size++;
}
// 出栈
void StackPop(ST* ps)
{
	assert(ps);
	assert(ps->size);

	ps->size--;
}
// 返回栈顶元素
StackDateType StackTop(ST* ps)
{
	assert(ps);

	return ps->a[ps->size - 1];
}
// 栈的元素个数
int StackSize(ST* ps)
{
	assert(ps);

	return ps->size;
}
// 栈是否为空
bool StackEmpty(ST* ps)
{
	assert(ps);

	return (ps->size == 0);
}

typedef struct {
	ST s1;
	ST s2;
} MyQueue;


MyQueue* myQueueCreate() {
	MyQueue* obj = (MyQueue*)malloc(sizeof(MyQueue));

	StackInit(&obj->s1);
	StackInit(&obj->s2);

	return obj;
}

void myQueuePush(MyQueue* obj, int x) {
	StackPush(&obj->s1, x);
}

int myQueuePop(MyQueue* obj) {
	if (StackEmpty(&obj->s2))
	{
		while (!StackEmpty(&obj->s1))
		{
			StackPush(&obj->s2, StackTop(&obj->s1));
			StackPop(&obj->s1);
		}
	}

	int ret = StackTop(&obj->s2);

	StackPop(&obj->s2);

	return ret;

}

int myQueuePeek(MyQueue* obj) {

	if (StackEmpty(&obj->s2))
	{
		while (!StackEmpty(&obj->s1))
		{
			StackPush(&obj->s2, StackTop(&obj->s1));
			StackPop(&obj->s1);
		}
	}
	int ret = StackTop(&obj->s2);

	return ret;
}

bool myQueueEmpty(MyQueue* obj) {
	return StackEmpty(&obj->s1) && StackEmpty(&obj->s2);
}

void myQueueFree(MyQueue* obj) {
	StackDestory(&obj->s1);
	StackDestory(&obj->s2);
	free(obj);
}