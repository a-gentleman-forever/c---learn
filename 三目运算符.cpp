#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;
// 返回的是一个变量，可以继续进行赋值操作
int main()
{
	int a = 10;
	int b = 20;
	int c = (a > b ? a : b);

	(a > b ? a : b) = 100;

	cout << c << endl;
	cout << b << endl;

	return 0;
}