#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

//class Time
//{
//public:
//	Time(int hour)
//	{
//		_hour = hour;
//	}
//
//private:
//	int _hour;
//};
//
//class Date
//{
//public:
//	//Time 没有默认构造函数 就只能用初始化列表
//	//初始化列表推荐使用初始化列表
//	//初始化列表课可以认为是成员变量的地方
//	Date(int month, int hour, int& x)
//		:_t(hour)
//		, _N(10)
//		, _ref(x)
//	{
//		_month = month;
//		//Time t(hour);
//		//_t = t;
//	}
//private:
//	int _month = 0; //C++11 缺省值
//	Time _t;
//	const int _N;
//	int& _ref;
//};
//
//int main()
//{
//	int y = 100;
//	Date(6, 1, y);
//
//	return 0;
//}

// explicit + 匿名对象
//class Date
//{
//public:
//	Date(int year)
//		:_year(year)
//	{
//		cout << "Date(int year)" << endl;	
//	}
//
//	Date(const Date& d)
//	{
//		cout << "Date(const Date& d)" << endl;
//	}
//
//	~Date()
//	{
//		cout << "~Date()" << endl;
//	}
//private:
//	int _year;
//};
//
//int main()
//{
//	Date d1(2022);// 直接调用构造
//	Date d2 = 2022;// 隐式类型转换：构造 + 拷贝构造 + 编译器优化 -> 直接调用构造
//
//	//匿名对象 --生命周期只在这一行
//	//Date(2000);
//
//
//	return 0;
//}

//class A
//{
//public:
//	A() { ++_scount; }
//
//	//静态成员函数 --没有this指针，不能访问非静态的成员
//	static int GetCount()
//	{
//		return _scount;
//	}
//
//	A(const A& t) { ++_scount; }
//private:
//	//静态成员变量 属于整个类，生命周期在整个程序运行期间
//	static int _scount; //声明
//};
//
////静态成员只能在类外面初始化
//int A::_scount = 0;
//
//int main()
//{
//	A a1;
//	A a2;
//
//	cout << a1.GetCount() << endl;
//
//	return 0;
//}

// 设计一个类要求只能在栈上定义
//class StackOnly
//{
//public:
//	static StackOnly CreatObj()
//	{
//		StackOnly so;
//		return so;
//	}
//
//private:
//	StackOnly(int x = 0, int y = 0)
//		:_x(y)
//		,_y(x)
//	{
//		
//	}
//private:
//	int _x = 0;
//	int _y = 0;
//};
//
//int main()
//{
//	//StackOnly so1; // 栈区
//	//static StackOnly so2; // 静态区
//	StackOnly so = StackOnly::CreatObj();
//
//	return 0;
//}

// 内部类
class A
{
private:
	int h;
	static int k;
public:
	class B // 内部类 定义在A的里面：1.受到A的类域限制，访问限定符 2.B天生是A的友元
	{
	public:
		void foo(const A& a)
		{
			cout << a.h << endl;
			cout << k << endl;
		}
	private:
		int _b;
	};
};
int A::k = 1;

class ListNode
{
private:
	ListNode* _next;
	int _val;
};

class List
{
public:
	void PushBack()
	{
		
	}
private:
	ListNode* _head;
};

int main()
{
	cout << sizeof(A) << endl; // 4	
	A a1;
	A::B b1;

	return 0;
}