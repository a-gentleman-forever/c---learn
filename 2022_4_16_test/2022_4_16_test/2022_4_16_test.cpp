#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

//class Person
//{
//public:
//	Person()
//	{
//		cout << "Person 构造函数的调用" << endl;
//	}
//
//	~Person()
//	{
//		cout << "Person 析构函数的调用" << endl;
//	}
//};
//int main() noexcept
//{
//	if (1)
//	{
//		Person p1;
//	}
//
//	system("pause");
//
//return 0;
//}

//class Person
//{
//
//
//public:
//	Person()
//	{
//		cout << "无参构造函数" << endl;
//	}
//
//	Person(int a)
//	{
//		m_Age = a;
//		cout << "有参构造函数" << endl;
//	}
//
//	Person(const Person &p)
//	{
//		m_Age = p.m_Age;
//		cout << "拷贝构造函数" << endl;
//	}
//	// 年龄
//	int m_Age;
//};
//int main()
//{
	//if (1)
	//{
	//	Person p1;
	//	Person p2(10);
	//	Person p3(p2);

	//	cout << p3.m_Age << endl;
	//}

	//if (1)
	//{
	//	Person p1;
	//	Person p2 = Person(10);
	//	Person p3 = Person(p2);

	//	Person(10); // 匿名对象 这一行结束后就会被销毁
	//}
//
//	if (1)
//	{
//		Person p1;
//		Person p2 = 10;
//		Person p3 = p2;
//	}
//
//	system("pause");
//	 
//	return 0;
//}

class Person
{
public:

	Person()
	{
		cout << "1" << endl;
	}

	Person(int a)
	{
		m_Age = a;
		cout << "2" << endl;
	}

	Person(const Person &p)
	{
		m_Age = p.m_Age;
		cout << "3" << endl;
	}

	int m_Age;
};

void Doword(Person p)
{

}

Person dowork2()
{
	Person p1;

	return p1;
}
int main()
{
	//Person p1;
	//Doword(p1);
	Person p1 = dowork2();
	
	system("pause");

	return 0;
}