#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <string>

using namespace std;

//1.c风格
//2.C++风格 string 变量名
int main()
{
	char ch1[] = "abcde";
	string ch2 = "bcdefrg";

	ch2 = "sdf";

	cout << ch1 << endl;
	cout << ch2 << endl;

	return 0;
}