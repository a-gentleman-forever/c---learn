#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<graphics.h>
#include<mmsystem.h>//win32 多媒体设备接口
#pragma comment(lib, "winmm.lib")
using namespace std;

#define WIN_WIDTH 400
#define WIN_HIGHT 701
#define BULLET_NUM 15 //最大子弹数量

IMAGE backGround;//背景图片
IMAGE player[2];
IMAGE bullet1[2];

struct Plane
{
	int x;
	int y;
	bool flag;//是否存活
}_player, bullet[BULLET_NUM];
//分模块处理
//加载图片
void GameInit()
{
	//播放音乐
	mciSendString(L"open D:/appt存放/天天酷跑项目资源/res/bg.mp3 alias BGM", 0, 0, 0);//向多媒体设备接口发送字符串 media device interface 
	mciSendString(L"play BGM repeat", 0, 0, 0);
	//加载背景图片
	loadimage(&backGround, L"D:/a图片/QQ/20210703183137_903d2.jpeg");
	loadimage(&player[0], L"D:/a图片/QQ/哈哈.png", 80, 120);
	loadimage(&player[1], L"D:/a图片/QQ/20210601113546_b1ff2.jpeg", 80, 120);
	loadimage(&bullet1[0], L"D:/a图片/QQ/子弹.png");
	//loadimage(&player[1], L"D:/a图片/QQ/20210601113546_b1ff2.jpeg", 80, 120);
	//初始化玩家数据
	_player.x = WIN_WIDTH / 2;
	_player.y = WIN_HIGHT - 120;
	_player.flag = true;
	//初始化子弹
	for (int i = 0; i < BULLET_NUM; i++)
	{
		bullet[i].flag = false;
		bullet[i].x;
		bullet[i].y;
	}
}

void GameDrow()
{
	//绘制背景
	putimage(0, 0, &backGround);
	//绘制玩家
	putimage(_player.x, _player.y, &player[0] /*NOTSRCERASE*/);
	//绘制子弹
	for (int i = 0; i < BULLET_NUM; i++)
	{
		if (bullet[i].flag == true) 
		putimage(bullet[i].x, bullet[i].y, &bullet1[0]);
	}
	
	//putimage(200, 200, &player[1], SRCINVERT);
}

void CreateBullet()
{
	for (int i = 0; i < BULLET_NUM; i++)
	{
		if (bullet[i].flag == false)
		{
			bullet[i].flag = true;
			bullet[i].x = _player.x + 40;
			bullet[i].x = _player.y;
			break;
		}
	}
}

void BulletMove(int speed)
{
	for (int i = 0; i < BULLET_NUM; i++)
	{
		if (bullet[i].flag == true)
		{
			bullet[i].y -= speed;
		}
	}
}

void GameControl(int speed)
{
	//if (_player.x - speed < 0 || _player.y - speed  < 0 || _player.x + speed  > WIN_WIDTH || _player.y + speed  > WIN_HIGHT)
	//{
	//	
	//}
	//_getch();GetAnsyncKey();
	if (GetAsyncKeyState(VK_UP))
	{
		_player.y -= speed;
	}
	if (GetAsyncKeyState(VK_DOWN))
	{
		_player.y += speed;
	}	
	if (GetAsyncKeyState(VK_LEFT))
	{
		_player.x -= speed;
	}	
	if (GetAsyncKeyState(VK_RIGHT))
	{
		_player.x += speed;
	}
	//发射子弹
	if (GetAsyncKeyState(VK_SPACE))
	{
		CreateBullet();
	}
	BulletMove(2);
}

int main()
{
	initgraph(WIN_WIDTH, WIN_HIGHT);//创建一个图像窗口
	GameInit();
	BeginBatchDraw();//开启双缓冲绘图
	while (1)
	{
		GameDrow();
		FlushBatchDraw();//我想看了
		GameControl(1);
	}
	EndBatchDraw();//结束

	return 0;
}