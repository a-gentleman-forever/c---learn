#pragma once

#include <iostream>
#include <string>
#include <list>
using namespace std;

// 人物类
class Person
{
public:
	Person(string name = "无", int age = 0, string tel = "00000000000")
		:_name(name)
		,_age(age)
		,_tel(tel)
	{
		WriteInfo();
	}

	void WriteInfo()
	{
		cout << "请输入姓名：";
		cin >> _name;

		cout << "请输入年龄：";
		cin >> _age;

		cout << "请输入电话：";
		cin >> _tel;
	}

	void Print()
	{
		cout << "姓名：" << _name << endl;
		cout << "年龄：" << _age << endl;
		cout << "电话：" << _tel << endl;
	}

private:
	string _name; //姓名
	int _age;	  //年龄
	string _tel;  //电话
};

//struct东西分类结构
struct Thing
{
	string _classname = "空";
	list<string> _objlist;

	void Print()
	{
		cout << "类名：" << _classname << endl;
		list<string>::iterator it = _objlist.begin();
		while (it != _objlist.end())
		{
			cout << *it << endl;
			++it;
		}
	}

	Thing(string classname)
		:_classname(classname)
	{
	}
};

//容器类
class contain
{
public:
	contain(size_t No = 0)
		:_No(No)
	{

	}
	//打印全部类别
	void Print()
	{
		cout << "编号：" << _No << endl;

		list<Thing*>::iterator it = _list_obj.begin();
		while (it != _list_obj.end())
		{
			(*it)->Print();
			++it;
		}
		cout << endl;
	}

	//获得No
	size_t GetNo()
	{
		return _No;
	}

	//增加类别
	void AddClass(string name)
	{
		Thing* t = new Thing(name);
		_list_obj.push_back(t); //增加类名
	}

	//增加某一类别中的物品
	void Addobj(string classname, string objname)
	{

	}

private:
	size_t _No = 0;
	list<Thing*> _list_obj; // 内容列表
	//Person _p; //人物信息
};

class AllCon
{
public:
	void AddCon()
	{
		contain* con = new contain(_con_list.size());
		_con_list.push_back(con); // 增加柜子
	}

	//打印No柜子的信息
	void Print(int No)
	{
		list<contain*>::iterator it = _con_list.begin();
		while (it != _con_list.end())
		{
			if ((*it)->GetNo() == No)
			{
				(*it)->Print();
				break;
			}
			++it;
		}
	}

	//增加NO柜子的物品类别
	void AddClass(int No, string name)
	{
		list<contain*>::iterator it = _con_list.begin();
		while (it != _con_list.end())
		{
			if ((*it)->GetNo() == No)
			{
				(*it)->AddClass(name);
				break;
			}
		}
	}

	//柜子数量
	size_t size()
	{
		return _con_list.size();
	}

	//检测是否存在
	bool ConEmpty()
	{
		return _con_list.empty();
	}

private:
	list<contain*>  _con_list;
};
