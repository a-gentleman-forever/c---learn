#define  _CRT_SECURE_NO_WARNINGS 1

#include "manage.h"
#include "do it.h"

//开始选择菜单
void menu()
{
	cout << "***************************************************" << endl;
	cout << "*****1.增加柜子*************************************" << endl;
	cout << "*****2.删除柜子************************************" << endl;
	cout << "*****3.打印柜子信息*********************************" << endl;
	cout << "*****4.添加柜子信息*********************************" << endl;
	cout << "***************************************************" << endl;
}

//柜子信息修改选择菜单
void con_menu()
{
	cout << "***************************************************" << endl;
	cout << "*****1.添加柜子资源类别******************************" << endl;
	cout << "*****2.删除柜子资源类别******************************" << endl;
	cout << "*****3.打印该柜子信息********************************" << endl;
	cout << "*****4.设置柜子所属人********************************" << endl;
	cout << "***************************************************" << endl;
}

int main()
{
	AllCon All;
	int option = 0; 
	string str;
	size_t no = 0;
	while (1)
	{
		menu();

		cin >> option;
		switch (option)
		{
		case 1: 
			All.AddCon();
			break;
		case 2: 
			cout << "输入2" << endl;
			break;
		case 3:
			// 打印所有柜子信息
			for (size_t i = 0; i < All.size(); ++i)
			{
				All.Print(i);
			}
			break;
		case 4:
			// 添加柜子信息
			cout << "需要进行修改的柜子编号：";
			cin >> no;
			if (All.size() - 1 < no)
			{
				cout << "查询错误，无该序列柜子" << endl;
			}
			else
			{
				con_menu();
			}
			break;
		default: 
			cout << "输入错误" << endl;
			break;
		}
	}

	return 0;
}