﻿#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <graphics.h>
#include <easyx.h>
using namespace std;

//创建窗口
//打开：initgraph(int x, int y, int style);
//关闭：closegraph();
//颜色设置
/*
	setbkcolor(颜色宏);
	颜色宏：颜色的英语大写单词
	RGB设置

	刷新
	cleardevice();

	
	line(int x, int y，int x, int y);划线
	circle(int x, int y, int r);画⚪
	setfill();设置填充颜色
	fillcircle();带线
	solidcircle();不带线
	rectangle(int x, int y, int xx, int yy);画矩形,左上角，右下角
	fillrectangle();
	solidrectangle();

	贴图
	原样贴图
		IMAGE变量去表示图片
		loadimage(IMAGE* img, URL);//加载图像
		loadimage(IMAGE* img, URL, int width, int height);//缩放加载图像
		putimage(int x, int y ,IMAGE* img);//显示图像
	透明贴图
		通过图像的颜色二进制运算达到去背景的效果图
		素材：掩码图，背景图
		按照特定方式贴图即可
		SRCAND 贴掩码图
		SRCPAINT 贴背景图
	png贴图

*/

void test_1()
{
	initgraph(800, 600);
	setbkcolor(RED);
	cleardevice();

	line(0, 0, 800, 600);
	rectangle(200, 200, 400, 400);
}

void Draw()
{
	//画棋盘
	initgraph(400, 400);
	setbkcolor(WHITE);
	cleardevice();
	setlinecolor(BLACK);

	for (int i = 0; i <= 400; i+= 20)
	{
		line(0, i, 400, i);
	}

	for (int i = 0; i <= 400; i += 20)
	{
		line(i, 0, i, 400);
	}
}

void test_2()
{
	initgraph(800, 600);
	IMAGE img;
	IMAGE img2;

	loadimage(&img, L"D:/a图片/博客/1.jpg");
	loadimage(&img2,  L"D:/a图片/QQ/子弹.png");
	putimage(0, 0, &img);
	putimage(100, 0, &img2);
}
int main()
{
	//Draw();
	test_2();

	while (1);
	closegraph();

	return 0;
}