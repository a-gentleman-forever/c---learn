#pragma once

namespace mr
{
	template<class T>
	struct list_node
	{
		T _data;
		list_node<T>* _next;
		list_node<T>* _prev;

		list_node(const T& x = T())
			:_data(x),
			_next(nullptr),
			_prev(nullptr)
		{

		}
	};

	template<class T, class Ref, class Ptr>
	struct __list_iterator
	{
		typedef list_node<T> Node;
		typedef __list_iterator<T, T&, T*> iterator;
		typedef __list_iterator<T, const T&, const T*> const_iterator;
		
		Node* _node;

		__list_iterator(Node* node)
			:_node(node)
		{
			
		}

		bool operator!=(const iterator& it) const
		{
			return _node != it._node;
		}

		T& operator*()
		{
			return _node->_data;
		}

		T* operator->()
		{
			return &(operator*());
		}

		iterator& operator++()
		{
			_node = _node->_next;

			return *this;
		}

		iterator operator++(int)
		{
			Node tmp = *this;
			_node = _node->_next;

			return tmp;
		}
	};

	template<class T>
	class list
	{
		typedef list_node<T> Node;
	public:
		typedef __list_iterator<T, T&, T*> iterator;
		typedef __list_iterator<T, const T&, const T*> const_iterator;

		const_iterator begin() const
		{
			return const_iterator(_head->_next);
		}

		const_iterator end() const
		{
			return const_iterator(_head);
		}

		iterator begin()
		{
			return iterator(_head->_next);
		}

		iterator end()
		{
			return iterator(_head);
		}

		list()
		{
			_head = new Node;
			_head->_next = _head;
			_head->_prev = _head;
		}

		void push_back(const T& x)
		{
			Node* tail = _head->_prev;
			Node* newnode = new Node(x);
			
			newnode->_next = _head;
			newnode->_prev = tail;

			tail->_next = newnode;
			_head->_prev = newnode;
		}

	private:
		Node* _head;
	};


	void test_list_1()
	{
		list<int> lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		lt.push_back(5);

		list<int>::iterator it = lt.begin();
		while (it != lt.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;

	}
}