#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <list>
#include <algorithm>
using namespace std;

#include "list.h"

void test_list_1()
{
	list<int> lt;
	
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);

	list<int>::iterator it = lt.begin();
	//while (it != lt.end())
	//{
	//	cout << *it << " ";
	//	++it;
	//}
	//cout << endl;

	while (it != lt.end())
	{
		*it *= 2;
		cout << *it << " ";
		++it;
	}
	cout << endl;

	lt.push_front(10);
	lt.push_front(20);
	lt.push_front(30);
	lt.push_front(40);

	lt.pop_back();
	lt.pop_back();
	lt.pop_back();
	lt.pop_back();
	lt.pop_back();

	for (auto& e : lt)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_list_2()
{
	list<int> lt;

	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);

	auto pos = find(lt.begin(), lt.end(), 3);

	lt.insert(pos, 10);

	for (auto& e : lt)
	{
		//没有迭代器失效的问题
		cout << e << " ";
	}
	cout << endl;

	pos = find(lt.begin(), lt.end(), 6);

	if (pos != lt.end())
	{
		//可能会
		lt.erase(pos);
	}
	
	
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_list_3()
{
	list<int> lt;

	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);
	lt.push_back(5);
	lt.push_back(5);

	lt.remove(5);

	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;
}

int main()
{
	//test_list_1();
	//test_list_2();
	//test_list_3();
	//mr::test_list_1();

	return 0;
}