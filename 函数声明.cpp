#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

int Max(int, int); 

int main()
{
	cout << Max(10, 20) << endl;

	return 0;
}

int Max(int x, int y)
{
	return x > y ? x : y;
}