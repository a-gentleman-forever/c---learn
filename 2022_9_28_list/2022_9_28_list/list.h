#pragma once

#include <iostream>
#include <assert.h>
using namespace std;

namespace mr
{
	// 节点的定义
	template<class T>
	struct ListNode
	{
		ListNode(const T& val = T())
		{
			_val = val;
		}

		ListNode<T>* _next;
		ListNode<T>* _prev;
		T _val;
	};

	//list_iterator 迭代器模板
	template<class T, class Ref, class Ptr>
	class Listiterator
	{
		typedef ListNode<T>* PNode;
		typedef Listiterator<T, Ref, Ptr> Self;
	public:
		Listiterator(PNode node = nullptr)
			:_head(node)
		{

		}

		Listiterator(const Self& I)
		{
			_head = I._head;
		}

		T& operator*()
		{
			return _head->_val;
		}
		
		Self& operator++()
		{
			_head = _head->_next;

			return *this;
		}

		bool operator!=(const Self& I)
		{
			return _head != I._head;
		}

	private:
		PNode _head;
	};

	//list模板类
	template<class T>
	class list
	{
		typedef ListNode<T> Node;
		typedef Node* PNode;
	public:
		typedef Listiterator<T, const T*, const T&> iterator;

		iterator begin()
		{
			iterator it(_head->_next);
			return it;
		}

		iterator end()
		{
			iterator it(_head);
			return it;
		}
	public:
		//构造
		list()
		{
			_head = new Node;
			_head->_next = _head;
			_head->_prev = _head;
		}

		list(int n, const T& value = T())
		{
			_head = new Node;
			_head->_next = _head;
			_head->_prev = _head;

			for (int i = 0; i < n; i++)
			{
				push_back(value);
			}
		}

		template <class Iterator>
		list(Iterator first, Iterator last)
		{
			_head = new Node;
			_head->_next = _head;
			_head->_prev = _head;

			while (first != last)
			{
				push_back(*first);

				++first;
			}
		}

		list(const list<T>& l)
		{
			list<T>::iterator it = l.begin();
			
			while (it != l.end())
			{
				push_back(*it);
			}

		}

		list<T>& operator=(const list<T> l);
		//析构
		~list()
		{
			PNode cur = _head->_next;
			PNode next = cur->_next;
			while (cur != _head)
			{
				delete cur;
				cur = next;
				next = next->_next;
			}
			delete cur;

			_head = nullptr;
		}

		//插入
		void push_back(const T& x)
		{
			assert(_head);

			PNode cur = _head->_prev;
			PNode tmp = new Node;
			tmp->_val = x;
			
			cur->_next = tmp;
			tmp->_prev = cur;

			_head->_prev = tmp;
			tmp->_next = _head;
		}

		void pop_back()
		{
			assert(_head->_prev != _head);
	
			PNode tmp = _head->_prev;
			PNode tail = tmp->_prev;

			delete tmp;

			_head->_prev = tail;
			tail->_next = _head;
		}

	private:
		//头节点成员 双向带头节点
		PNode _head;
	};

	void test_1()
	{
		list<int> lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		lt.push_back(5);

		list<int>::iterator it = lt.begin();
		while (it != lt.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;

		lt.pop_back();

		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void test_2()
	{
		list<int> lt(5, 8);

		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;

		list<int> lt1(lt.begin(), lt.end());
		
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;
	}
}