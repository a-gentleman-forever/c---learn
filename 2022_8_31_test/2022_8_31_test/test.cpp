#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int singleNumber(vector<int>& nums) {
        // O(N^2)
        for (int i = 0; i < nums.size(); ++i)
        {
            int count = 0;
            for (int j = 0; j < nums.size(); ++j)
            {
                if (nums[i] == nums[j])
                {
                    count++;
                }
            }

            if (count == 1)
            {
                return nums[i];
            }
        }

        return -1;
    }
};

int main()
{


	return 0;
}