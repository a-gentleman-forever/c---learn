#define  _CRT_SECURE_NO_WARNINGS 1
//多态
#include <iostream>
using namespace std;

//class Person
//{
//public:
//	virtual void BuyTicket() { cout << "买票-全价" << endl; }
//};
//
//class Student : public Person
//{
//public:
//	// 虚函数的重写/覆盖 函数名参数返回值都相同
//	virtual void BuyTicket() { cout << "买票-半价" << endl; }
//};
//
//class Soldier : public Person
//{
//public:
//	virtual void BuyTicket() { cout << "优先买票" << endl; }
//};
//
//// 多态的两个条件：
//// 1、虚函数
//// 2、父类的指针或者引用去调用虚函数
//
//void Func(Person &p)
//{
//	p.BuyTicket();
//}
//
//int main()
//{
//	Person ps;
//	Student st;
//	Soldier sd;
//
//	Func(ps);
//	Func(st);
//	Func(sd);
//
//	return 0;
//}

//class Person
//{
//public:
//	virtual Person* BuyTicket() 
//	{
//		cout << "买票-全价" << endl; 
//		return this;
//	}
//};
//
//class Student : public Person
//{
//public:
//	// 虚函数的重写/覆盖 函数名参数返回值都相同
//	virtual Student* BuyTicket() 
//	{ 
//		cout << "买票-半价" << endl; 
//		return this;
//	}
//};
//
//// 多态的两个条件：
//// 1、虚函数
//// 2、父类的指针或者引用去调用虚函数
//
//void Func(Person& p)
//{
//	p.BuyTicket();
//}
//
//void Func(Person* p)
//{
//	p->BuyTicket();
//}
//
//int main(int argc, char* argv[])
//{
//	Person ps;
//	Student st;
//
//	Person* pps = new Person;
//
//	Func(ps);
//	Func(st);
//	Func(pps);
//
//	return 0;
//}

// 多态的原理
//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//private:
//	int _b = 1;
//};
//	
//int main()
//{
//	Base b;
//	cout << sizeof Base << endl;	
//
//	return 0;	
//} 

class YX
{
private:
	int _age = 0;
	const char* Sex = "男";
};

int main()
{


	return 0;
}



