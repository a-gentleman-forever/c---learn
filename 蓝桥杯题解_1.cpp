#define  _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

void C()
{
    int n = 0;
    cin >> n;
    int* arr = new int[n];

    for (int i = 0; i < n; ++i)
    {
        cin >> arr[i];
    }

    int s = 0;
    for (int i = 0; i < n - 1; ++i)
    {
        for (int j = i + 1; j < n; ++j)
        {
            s += arr[i] * arr[j];
        }
    }

    cout << s << endl;
}

void C()
{
    int n = 0;
    cin >> n;
    if (n == 0)
    {
        cout << 0 << endl;
        return 0;
    }
    long long* arr = new long long[n];

    for (int i = 0; i < n; ++i)
    {
        cin >> arr[i];
    }

    long long* rarr = new long long[n];

    rarr[0] = arr[0];
    for (int i = 1; i < n; ++i)
    {
        rarr[i] = rarr[i - 1] + arr[i];
    }

    long long s = 0;
    for (int i = 0; i < n - 1; ++i)
    {
        s += arr[i] * (rarr[n - 1] - rarr[i]);
    }

    cout << s << endl;

}

int main()
{


	return  0;
}