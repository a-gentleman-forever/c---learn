#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

class Person
{
public:
	void print()
	{
		cout << "name:" << _name << endl;
		cout << "age:" << _age << endl;
	}

public:
//private:
	string _name = "peter"; //姓名
	int _age = 18; //年龄
};

class Student :public Person
{
public:
	int _stuid; //学号
	int _major; //专业  
};

class Teacher :public Person
{
public:
	int _jobid; //工号
};

int main()
{
	Student s;
	//s._name = "小张";
	s.print();

	Teacher t;
	//t._name = "老吴";
	t.print();

	return 0;
}