#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
using namespace std;

class Date
{
public:
	void Print()
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}
	
	// 获取某年某月的天数
	//会频繁调用，直接放在类里面定义作为内联
	int GetMonthDay(int year, int month)
	{
		static int days[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
		int day = days[month];

		if (month == 2 
			&& ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
		{
			day += 1;
		}
		
		return day;
	}

	// 全缺省的构造函数
	Date(int year = 1900, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	// 拷贝构造函数
  // d2(d1)
	Date(const Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}

	// 赋值运算符重载
  // d2 = d3 -> d2.operator=(&d2, d3)
	Date& operator=(const Date& d)
	{
		if (this != &d)
		{
			_year = d._year;
			_month = d._month;
			_day = d._day;
		}

		return *this;
	}

	// 析构函数
	~Date()
	{

	}

	// 日期+=天数
	Date& operator+=(int day)
	{
		_day += day;
		while (_day > GetMonthDay(_year, _month))
		{
			_day -= GetMonthDay(_year, _month);
			_month++;
			if (_month > 12)
			{
				_year++;
				_month = 1;
			}
		}

		return *this;
	}
	// 日期+天数
	Date operator+(int day)
	{
		Date d(*this);
		d += day;

		return d;
	}
	// 日期-天数
	Date operator-(int day)
	{
		Date d(*this);
		d -= day;

		return d;
	}
	// 日期-=天数
	Date& operator-=(int day)
	{
		_day -= day;
		while (_day <= 0)
		{
			_day += GetMonthDay(_year, _month);
			_month--;
			if (_month <= 0)
			{
				_year--;
				_month = 12;
			}
		}

		return *this;
	}
	// 前置++
	Date& operator++()
	{
		*this += 1;

		return *this;
	}
	// 后置++
	Date operator++(int)// 增加一个int参数，与前置++构成函数重载
	{
		Date d(*this);
		*this += 1;

		return d;
	}
	// 后置--
	Date operator--(int)
	{
		Date d(*this);
		*this -= 1;

		return d;
	}
	// 前置--
	Date& operator--()
	{
		*this -= 1;

		return *this;
	}
	// >运算符重载
	bool operator>(const Date& d)
	{
		if ((_year > d._year)
			|| (_year == d._year && _month > d._month)
			|| (_year == d._year && _month == d._month && _day > d._day))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	// ==运算符重载
	bool operator==(const Date& d)
	{
		if (_year == d._year && _month == d._month && _day == d._day)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	// >=运算符重载
	bool operator >= (const Date& d)
	{
		return (*this > d) || (*this == d);
	}
	// <运算符重载
	bool operator < (const Date& d)
	{
		return !(*this >= d);
	}
	// <=运算符重载
	bool operator<=(const Date& d)
	{
		return !(*this > d);
	}	
	// !=运算符重载
	bool operator!=(const Date& d)
	{
		return !(*this == d);
	}
	// 日期-日期 返回天数
	int operator-(const Date& d)
	{
		Date d1(*this);
		Date d2(d);

		int num = 0;
		if (d1 >= d2)
		{
			while (d2 != d1)
			{
				num++;
				d2._day++;
				if (d2._day > GetMonthDay(d2._year, d2._month))
				{
					d2._day = 1;
					d2._month++;

					if (d2._month > 12)
					{
						d2._year++;
						d2._month = 1;
					}
				}
			}
		}
		else 
		{
			while (d2 != d1)
			{
				num--;
				d1._day++;
				if (d1._day > GetMonthDay(d1._year, d1._month))
				{
					d1._day = 1;
					d1._month++;

					if (d1._month > 12)
					{
						d1._year++;
						d1._month = 1;
					}
				}
			}
		}

		return num;
	}

private:
	int _year;
	int _month;
	int _day;
};

class A
{
public:
	A()
	{
		cout << "A()" << endl;
	}

	A(A& a)
	{
		cout << "A(A&)" << endl;
	}

	~A()
	{
		cout << "~A()" << endl;
	}
private:
	int _a;
	int _c;
	int _b;
};

void test01()
{
	//Date d1(2022,7,1);
	//Date d2(2023,7,31);

	//int num = d1 - d2;
	//int num1 = d2 - d1;

	//cout << num << endl;
	//cout << num1 << endl;

	//A a1;
	//A a2 = a1;
}

void testDate1()
{
	Date d1(2022, 7, 27);
	Date d2(2022, 7, 28);
	Date d3(2021, 7, 29);

	cout << (d1 < d2) << endl;
	cout << (d1 < d3) << endl;
	cout << (d1 == d2) << endl;
	cout << (d1 > d3) << endl;		
}

void testDate2()
{
	Date d1(2022, 7, 24);

	d1 += 4;
	d1.Print();
	
	d1 += 40;
	d1.Print();

	d1 += 400;
	d1.Print();

	d1 += 4000;
	d1.Print();

	d1 -= 400000;
	d1.Print();

	Date d2(2022, 7, 24);
	Date d3(2023, 9, 1);
	cout << (d3 - d2) << endl;
}

int main()
{
	//testDate1();
	testDate2();

	return 0;
}



