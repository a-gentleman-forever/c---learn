#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

//// 析构函数的重写
//class Person
//{
//public:
//	~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//};
//
//class Student : public Person
//{
//public:
//	virtual ~Student()
//	{
//		cout << "~Student()" << endl;
//	}
//};
//
//int main()
//{
//	//Person p;
//	//Student s;
//
//	Person* ptr1 = new Person;
//	delete ptr1;
//
//	Person* ptr2 = new Student;
//	delete ptr2;
//
//	return 0; 
//}

//class Car
//{
//public:
//	virtual void Drive() final {}
//};
//
//class Benz : public Car
//{
//public:
//	virtual void Drive() { cout << "Benz-舒适" << endl; }
//};
//
//int main()
//{
//
//	return 0;
//}

//class Car
//{
//public:
//	virtual void Drive() {}
//};
//
//class Benz : public Car
//{
//public:
//	// 检查子类虚函数是否完成重写
//	virtual void Drive() override { cout << "Benz-舒适" << endl; }
//}; 
//
//int main()
//{
//
//	return 0;
//}

////抽象类
//class Car
//{
//public:
//	virtual void Drive() = 0;
//};
//
//class BWM : public Car
//{
//public:
//	void Drive()
//	{
//		cout << "BWM-控制" << endl;
//	}
//};
//
//class MyCar : public BWM
//{
//	virtual void Drive()
//	{
//		cout << "MyCar-改装车" << endl;
//	}
//};
//
//int main()
//{
//	//Car c;
//	Car* ptr = new BWM;
//	ptr->Drive();
//
//	ptr = new MyCar;
//	ptr->Drive();
//
//	return 0;
//}

//class Person
//{
//public:
//	virtual void BuyTicket() { cout << "买票-全价" << endl; }
//	virtual void Func1() { cout << "Person::Func1" << endl; }
//};
//
//class Student : public  Person
//{
//public:
//	virtual void BuyTicket() { cout << "买票-半价" << endl; }
//	virtual void Func2() { cout << "Student::Func2" << endl; }
//};
//
//typedef void(*VFtable)();
//
//void PrintVFtable(VFtable* table)
//{
//	for (size_t i = 0; table[i] != nullptr; ++i)
//	{
//		printf("table[%d]:%p->", i, table[i]);
//		VFtable pf = table[i];
//		pf();
//	}
//	cout << endl;
//}
//
//int main()
//{
//	// 同一个类型的对象共用一个虚表
//	Person p1;
//	Person p2;
//
//	// vs下	不管是否完成重写，子类的虚表和父类的虚表都不是同一个
//	Student s1;
//	Student s2;
//
//	PrintVFtable((VFtable*)*(int*)(&p1));
//	PrintVFtable((VFtable*)*(int*)(&s1));
//
//	return 0;
//}

typedef void(*VFtable)();

void PrintVFtable(VFtable* table)
{
	for (size_t i = 0; table[i] != nullptr; ++i)
	{
		printf("table[%d]:%p->", i, table[i]);
		VFtable pf = table[i];
		pf();
	}
	cout << endl;
}

class Base1
{
public:
	virtual void func1() { cout << "Base1::func1" << endl; }
	virtual void func2() { cout << "Base1::func2" << endl; }
private:
	int bi = 1;
};


class Base2
{
public:
	virtual void func1() { cout << "Base2::func1" << endl; }
	virtual void func2() { cout << "Base2::func2" << endl; }
private:
	int b2 = 2;
};


class Derive : public Base1, public Base2
{
public:
	virtual void func1() { cout << "Derive::func1" << endl; }
	virtual void func3() { cout << "Derive::func3" << endl; }
private:
	int d = 3;
};

int main()
{
	Derive d1;
	cout << sizeof Derive << endl;
	
	PrintVFtable((VFtable*)*(int*)(&d1));

	Base2* ptr = &d1;
	//PrintVFtable((VFtable*)*(int*)((char*)&d1 + sizeof(Base1)));
	PrintVFtable((VFtable*)*(int*)(ptr));

	return 0;
}

