#pragma once
#include <iostream>
using namespace std;

template<class K>
struct BSTreeNode
{
	BSTreeNode(const K& key)
		:_left(nullptr)
		, _right(nullptr)
		, _key(key)
	{

	}

	BSTreeNode<K>* _left;
	BSTreeNode<K>* _right;
	K _key;
};

//class BinarySearchTree
template<class k>
class BSTree
{
	typedef BSTreeNode<k> Node;
public:
	bool Insert(const k& key)
	{
		if (_root == nullptr)
		{
			_root = new Node(key);
			
			return true;
		}
		else
		{
			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (key < cur->_key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else if (key > cur->_key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else
				{
					return false;
				}
			}

			cur = new Node(key);
			if (parent->_key < key)
			{
				parent->_right = cur;
			}
			else
			{
				parent->_left = cur;
			}

			return true;
		}	
	}

	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}

	bool Find(const k& key)
	{
		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				return false;
			}
		}

		return false;
	}

	bool Erase(const k& key)
	{
		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			if (cur->_key < key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_key > key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				//开始删除
				//1.左为空
				//2.右为空
				//3.左右都不为空
				if (cur->_left == nullptr)
				{
					if (cur == _root)
					{
						_root = cur->_right;
					}
					else
					{
						if (cur == parent->_left)
						{
							parent->_left = cur->_right;
						}
						else
						{
							parent->_right = cur->_right;
						}
					}

					delete cur;
					cur = nullptr; 
				}
				else if (cur->_right == nullptr) 
				{
					if (cur == _root)
					{
						_root = cur->_left;
					}
					else
					{
						if (cur == parent->_left)
						{
							parent->_left = cur->_left;
						}
						else
						{
							parent->_right = cur->_left;
						}
					}

					delete cur;
					cur = nullptr;
				}
				else
				{
					// 替换法删除
					Node* minParent = cur;
					Node* min = cur->_right;
					while (min->_left)
					{
						minParent = min;
						min = min->_left;
					}

					swap(cur->_key, min->_key);
					if (minParent->_left == min)
					{
						minParent->_left = min->_right;
					}
					else
					{
						minParent->_right = min->_right;
					}
					
					delete min;
					min = nullptr;
				}

				return true;
			}
		}

		return false;
	}

	~BSTree()
	{
		_Destory(_root);
	}

	//C++11:强制编译器生成默认构造
	BSTree() = default; 

	BSTree(const BSTree<k>& t)
	{
		_Copy(t._root);
	}

///////////////////////////////////////////////////////////////////////////////
	bool FindR(const k& key)
	{
		return _FindR(_root, key);
	}

	bool InsertR(const k& key)
	{
		return _InsertR(_root, key);
	}

	bool EraseR(const k& key)
	{
		return _EraseR(_root, key);
	}

	BSTree<k>& operator=(BSTree<k> t)
	{
		swap(_root, t._root);
		return *this;
	}

private:
	Node* _Copy(Node* root)
	{
		if (root == nullptr)
		{
			return nullptr;
		}

		Node* copyRoot = new Node(root->_key);
		copyRoot->_left = _Copy(root->_left);
		copyRoot->_right = _Copy(root->_right);
		return copyRoot;
	}

	void _Destory(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}

		_Destory(root->_left);
		_Destory(root->_right);
		delete root;
		root = nullptr;
	}

	bool _EraseR(Node*& root, const k& key)
	{
		if (root == nullptr)
		{
			return false;
		}

		if (root->_key < key)
		{
			return _EraseR(root->_right, key);
		}
		else if (root->_key > key)
		{
			return _EraseR(root->_left, key);
		}
		else
		{
			Node* del = root;
			if (root->_left == nullptr)
			{
				root = root->_right;
			}
			else if (root->_right == nullptr)
			{
				root = root->_left;
			}
			else
			{
				//找右数的最左节点
				Node* min = root->_right;
				while (min->_left)
				{
					min = min->_left;
				}

				swap(root->_key, min->_key);
				return _InserR(root->_right, key);
			}

			delete del;
			return true;
		}
	}

	bool _InsertR(Node*& root,const k& key)
	{
		if (root == nullptr)
		{
			root = new Node(key);
			return true;
		}
			
		if (root->_key < key)
		{
			return _InsertR(root->_right, key);
		}
		else if (root->_key > key)
		{
			return _InsertR(root->_left, key);
		}
		else
		{
			return false;
		}
	}

	bool _FindR(Node* root, const k& key)
	{
		if (root == nullptr)
		{
			return false;
		}
		
		if (root->_key < key)
		{
			return _FindR(root->_right);
		}
		else if (root->_key > key)
		{
			return _FindR(root->_left);
		}
		else
		{
			return true;
		}
	}

	void _InOrder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}

		_InOrder(root->_left);
		cout << root->_key << " ";	
		_InOrder(root->_right);
	}

private:
	Node* _root = nullptr;
};

namespace mr
{
	template<class K, class v>
	struct BSTreeNode
	{
		BSTreeNode(const K& key, const v& value)
			:_left(nullptr)
			, _right(nullptr)
			, _key(key)
			,_value(value)
		{

		}

		BSTreeNode<K, v>* _left;
		BSTreeNode<K, v>* _right;
		K _key;
		v _value;
	};

	//class BinarySearchTree
	template<class k, class v>
	class BSTree
	{
		typedef BSTreeNode<k, v> Node;
	public:
		bool Insert(const k& key, const v& value)
		{
			if (_root == nullptr)
			{
				_root = new Node(key, value);

				return true;
			}
			else
			{
				Node* parent = nullptr;
				Node* cur = _root;
				while (cur)
				{
					if (key < cur->_key)
					{
						parent = cur;
						cur = cur->_left;
					}
					else if (key > cur->_key)
					{
						parent = cur;
						cur = cur->_right;
					}
					else
					{
						return false;
					}
				}

				cur = new Node(key, value);
				if (parent->_key < key)
				{
					parent->_right = cur;
				}
				else
				{
					parent->_left = cur;
				}

				return true;
			}
		}

		void InOrder()
		{
			_InOrder(_root);
			cout << endl;
		}

		Node* Find(const k& key)
		{
			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (key < cur->_key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else if (key > cur->_key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else
				{
					//return false;
					return cur;
				}
			}

			return nullptr;
		}

		bool Erase(const k& key)
		{
			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					//开始删除
					//1.左为空
					//2.右为空
					//3.左右都不为空
					if (cur->_left == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_right;
						}
						else
						{
							if (cur == parent->_left)
							{
								parent->_left = cur->_right;
							}
							else
							{
								parent->_right = cur->_right;
							}
						}

						delete cur;
						cur = nullptr;
					}
					else if (cur->_right == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_left;
						}
						else
						{
							if (cur == parent->_left)
							{
								parent->_left = cur->_left;
							}
							else
							{
								parent->_right = cur->_left;
							}
						}

						delete cur;
						cur = nullptr;
					}
					else
					{
						// 替换法删除
						Node* minParent = cur;
						Node* min = cur->_right;
						while (min->_left)
						{
							minParent = min;
							min = min->_left;
						}

						swap(cur->_key, min->_key);
						if (minParent->_left == min)
						{
							minParent->_left = min->_right;
						}
						else
						{
							minParent->_right = min->_right;
						}

						delete min;
						min = nullptr;
					}

					return true;
				}
			}

			return false;
		}

		~BSTree()
		{
			_Destory(_root);
		}

		//C++11:强制编译器生成默认构造
		BSTree() = default;

		BSTree(const BSTree<k, v>& t)
		{
			_Copy(t._root);
		}

		///////////////////////////////////////////////////////////////////////////////
		bool FindR(const k& key)
		{
			return _FindR(_root, key);
		}

		bool InsertR(const k& key, const v& value)
		{
			return _InsertR(_root, key, value);
		}

		bool EraseR(const k& key)
		{
			return _EraseR(_root, key);
		}

		BSTree<k, v>& operator=(BSTree<k, v> t)
		{
			swap(_root, t._root);
			return *this;
		}

	private:
		Node* _Copy(Node* root)
		{
			if (root == nullptr)
			{
				return nullptr;
			}

			Node* copyRoot = new Node(root->_key, root->_value);
			copyRoot->_left = _Copy(root->_left);
			copyRoot->_right = _Copy(root->_right);
			return copyRoot;
		}

		void _Destory(Node* root)
		{
			if (root == nullptr)
			{
				return;
			}

			_Destory(root->_left);
			_Destory(root->_right);
			delete root;
			root = nullptr;
		}

		bool _EraseR(Node*& root, const k& key)
		{
			if (root == nullptr)
			{
				return false;
			}

			if (root->_key < key)
			{
				return _EraseR(root->_right, key);
			}
			else if (root->_key > key)
			{
				return _EraseR(root->_left, key);
			}
			else
			{
				Node* del = root;
				if (root->_left == nullptr)
				{
					root = root->_right;
				}
				else if (root->_right == nullptr)
				{
					root = root->_left;
				}
				else
				{
					//找右数的最左节点
					Node* min = root->_right;
					while (min->_left)
					{
						min = min->_left;
					}

					swap(root->_key, min->_key);
					return _InserR(root->_right, key);
				}

				delete del;
				return true;
			}
		}

		bool _InsertR(Node*& root, const k& key, const v& value)
		{
			if (root == nullptr)
			{
				root = new Node(key, value);
				return true;
			}

			if (root->_key < key)
			{
				return _InsertR(root->_right, key);
			}
			else if (root->_key > key)
			{
				return _InsertR(root->_left, key);
			}
			else
			{
				return false;
			}
		}

		bool _FindR(Node* root, const k& key)
		{
			if (root == nullptr)
			{
				return false;
			}

			if (root->_key < key)
			{
				return _FindR(root->_right);
			}
			else if (root->_key > key)
			{
				return _FindR(root->_left);
			}
			else
			{
				return true;
			}
		}

		void _InOrder(Node* root)
		{
			if (root == nullptr)
			{
				return;
			}

			_InOrder(root->_left);
			cout << root->_key << " ";
			_InOrder(root->_right);
		}

	private:
		Node* _root = nullptr;
	};

	void TestBSTree1()
	{
		BSTree<string, string> dict;

		dict.Insert("string", "字符串");
		dict.Insert("back", "回来");
		dict.Insert("home", "家");
		dict.Insert("come", "来");
		dict.Insert("insert","插入");

		string str;

		while (cin >> str)
		{
			BSTreeNode<string, string>* ret = dict.Find(str);
			if (ret)
			{
				cout << ":" << ret->_value << endl;
			}
			else
			{
				cout << "->" << "没有查询到该单词" << endl;
			}
		}
	}
}

void TestBSTree1()
{
	BSTree<int> t;
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	for (auto e : a)
	{
		t.Insert(e);	
	}

	//排序加去重
	t.InOrder();

	//t.Erase(8);
	//t.InOrder();

	//t.Erase(3);
	//t.InOrder();


	for (auto e : a) 
	{
		t.Erase(e);
		t.InOrder();
	}

	cout << endl;
}

void TestBSTree2()
{
	BSTree<int> t;

	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	for (auto e : a)
	{
		t.InsertR(e);
	}

	t.InOrder();

	cout << endl;
}