#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

//class Base1
//{
//public:
//	virtual void func1() { cout << "Base1::func1()" << endl; }
//	virtual void func2() { cout << "Base1::func2()" << endl; }
//private:
//	int b1 = 1;
//};
//
//class Base2
//{
//public:
//	virtual void func1() { cout << "Base2::func1()" << endl; }
//	virtual void func2() { cout << "Base2::func2()" << endl; }
//private:
//	int b2 = 2;
//};
//
//class Derive : public Base1, public Base2
//{
//public:
//	virtual void func1() { cout << "Derive::func1()" << endl; }
//	virtual void func3() { cout << "Derive::func3()" << endl; }
//private:
//	int d = 3;
//};
//
//typedef void(*VFtable)();
//
//void PrintVFtable(VFtable* table)
//{
//	for (size_t i = 0; table[i] != nullptr; ++i)
//	{
//		printf("VFtable[%d]:%p->", i, table[i]);
//		VFtable pf = table[i];
//		pf();
//	}
//	cout << endl;
//}
//
//int main()
//{
//	Derive d1;
//
//	// (VFtable*)*(int*)(&d1)
//	//PrintVFtable((VFtable*)*(int*)(&d1));
//
//	//Derive* ptr1 = &d1;
//	//PrintVFtable((VFtable*)*(int*)(ptr1));
//
//	//Base1* ptr2 = &d1;
//	//PrintVFtable((VFtable*)*(int*)(ptr2));
//
//	//Base2* ptr3 = &d1;
//	//PrintVFtable((VFtable*)*(int*)(ptr3));
//
//	Base1* ptr1 = &d1;
//	Base2* ptr2 = &d1;
//
//	ptr1->func1();
//	ptr2->func1();
//
//	return 0;
//}
//
//class A
//{
//public:
//	virtual void func1()
//	{
//		cout << "A::func1()" << endl;
//	}
//public:
//	int _a;
//};
//
//class B : virtual public A
//{
//public:
//	virtual void func1()
//	{
//		cout << "B::func1()" << endl;
//	}
//
//	virtual void func2()
//	{
//		cout << "B::func2()" << endl;
//	}
//public:
//	int _b;
//};
//
//class C : virtual public A
//{
//public:
//	virtual void func1()
//	{
//		cout << "C::func1()" << endl;
//	}
//
//	virtual void func2()
//	{
//		cout << "C	::func2()" << endl;
//	}
//public:
//	int _c;
//};
////菱形继承导致，虚函数重写不明确
////D就必须进行虚函数重写
//class D : virtual public B, virtual public C
//{
//public:
//	virtual void func1()
//	{
//		cout << "D::func1()" << endl;
//	}
//public:
//	int _d;
//};
//
//int main()
//{
//
//
//	return 0;
//}

//int main()
//{
//
//
//	return 0;
//}

//class A
//{
//public:
//	int _a;
//};
//
//class B : public A
//{
//private:
//	int _b;
//};

//class Person
//{
//public:
//	Person(string name = "zs", int age = 0)
//		:_name(name)
//		,_age(age)
//	{
//		
//	}
//
//	void Print()
//	{
//		cout << _name << endl;
//		cout << _age << endl;
//	}
//
//public:
//	string _name;
//	int _age;
//};
//
//class Student : public Person
//{
//public:
//	Student(int No = 213310)
//		:_No(No)
//	{
//		
//	}
//
//	void Print()
//	{
//		cout << _name << endl;
//		cout << _age << endl;
//		cout << _No << endl;
//	}
//private:
//	int _No; //编号
//};
//
//int main()
//{
//	//Person p1;
//	//Student s1;
//
//	//s1.Print();
//
//	//p1._name = "张三";
//	//p1._age = 18;
//
//	//p1 = s1;
//	//p1.Print();
//
//	//Person p1;
//	//Student s1;
//
//	return 0;
//}
//
//class A
//{
//public:
//	void Print() { cout << "class A" << endl; }
//public:
//	int _a = 1;
//};
//
//class B : public A
//{
//public:
//	void Print(){	cout << "class B" << endl;	}
//public:
//	int _b = 2;
//};
//
//class C : public A
//{
//public:
//	void Print() { A::Print(); }
//public:
//	int _c = 2;
//};
//
//int main()
//{
//	B b;
//	b.Print(); // 调用b中重定义的Print()
//
//	C c;
//	c.Print(); // 通过c显示调用A中的Print()
//
//	return 0;
//}

//
//class A
//{
//public:
//	void Print() { cout << "class A" << endl; }
//public:
//	int _a = 1;
//};
//
//class B
//{
//public:
//	void Print() { cout << "class B" << endl; }
//public:
//	int _b = 2;
//};
//
//class C : public A, public B
//{
//public:
//	void Print() { A::Print(); }
//public:
//	int _c = 2;
//};

//
//class A
//{
//public:
//	void Print() { cout << "class A" << endl; }
//public:
//	int _a = 1;
//};
//
//class B : virtual public A
//{
//public:
//	void Print() { cout << "class B" << endl; }
//public:
//	int _b = 2;
//};
//
//class C : virtual public A
//{
//public:
//	void Print() { A::Print(); }
//public:
//	int _c = 3;
//};
//class D : public B, public C
//{
//public:
//	void Print() { A::Print(); }
//public:
//	int _d = 4;
//};
//
//int main()
//{
//
//	return 0;
//}

#include "BinarySearchTree.h"

int main()
{
	TestBSTree1();

	return 0;
}