#define  _CRT_SECURE_NO_WARNINGS 1

#include "Stack.h"

//初始化
void StackInit(ST* ps)
{
	ps->a = (STDateType*)malloc(sizeof(STDateType)*4);
	if (ps->a == NULL)
	{
		printf("malloc fail\n");
		exit(-1);
	}
	ps->top = 0;
	ps->capacity = 4;
}
//销毁
void StackDestory(ST* ps)
{
	free(ps->a);
	ps->a = NULL;
	ps->capacity = 0;
	ps->top = 0;
}
//压栈
void StackPush(ST* ps, STDateType x)
{
	assert(ps);
	//满了
	if (ps->top == ps->capacity)
	{
		STDateType* tmp = (STDateType*)realloc(ps->a, sizeof(STDateType)*(ps->capacity)*2);
		if (tmp == NULL)
		{
			printf("realloc fail\n");
			exit(-1);
		}
		ps->a = tmp;
	}
	ps->a[ps->top] = x;
	ps->top++;
}
//出栈
void StackPop(ST* ps)
{
	assert(ps);
	assert(ps->top > 0);
	ps->top--;
}
//返回栈顶元素
STDateType StackTop(ST* ps)
{
	assert(ps);
	assert(ps->top > 0);

	return ps->a[ps->top-1];
}
//栈的元素个数
int StackSize(ST* ps)
{
	assert(ps);

	return ps->top;
}
//栈是否为空
bool StackEmpty(ST* ps)
{
	assert(ps);

	return ps->top == 0;
}
