#define  _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
extern "C"
{
    #include "D:\a编程学习系列\c学习2019\Stack\Stack\Stack.h"
}
bool isValid(const char* s) {
    ST s1;
    StackInit(&s1);

    while (*s)
    {
        if (*s == '(' || *s == '{' || *s == '[')
        {
            StackPush(&s1, *s);
        }
        else
        {
            if (StackEmpty(&s1))
            {
                return false;
            }
            if ((StackTop(&s1) == '(' && *s == ')') || (StackTop(&s1) == '{' && *s == '}') || (StackTop(&s1) == '[' && *s == ']'))
            {
                StackPop(&s1);
            }
            else
            {
                return false;
            }
        }

        s++;
    }

    if (*s || !StackEmpty(&s1))
    {
        return false;
    }

    return true;
}

int main()
{
    printf("%d\n", isValid("{}{}{}"));
    
    return 0;
}