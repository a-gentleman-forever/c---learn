#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <stack>
#include <queue>
#include <list>
#include "Queue.h"
#include "Stack.h"
#include "PriorityQueue.h"
using namespace std;

void test_stack()
{
	//mr::Stack<int, vector<int>> st;
	//mr::Stack<int, list<int>> st;
	mr::Stack<int> st;

	st.push(1);
	st.push(2);
	st.push(3);
	st.push(4);

	while (!st.empty())
	{
		cout << st.top() << endl;
		st.pop();
	} 
}

void test_queue()
{
	//queue<int> q;
	mr::Queue<int> q;

	q.push(1);
	q.push(2);
	q.push(3);
	q.push(4);

	while (!q.empty())
	{
		cout << q.front() << endl;
		q.pop();
	}
}

void test_priority_queue()
{
	//queue<int> q;
	mr::priority_queue<int, deque<int>, greater<int>> q;

	q.push(1);
	q.push(2);
	q.push(3);
	q.push(4);

	while (!q.empty())
	{
		cout << q.top() << endl;
		q.pop();
	}
}


int main()		
{
	test_stack();
	cout << endl;
	test_queue();
	cout << endl;
	test_priority_queue( );
	
	return 0;
}

//仿函数 --类 重载了operator()
//类对象可以像函数一样去使用
//namespace mr
//{
//	template<class T>
//	struct less
//	{
//		bool operator()(const T& l, const T& r) const
//		{
//			return l < r;
//		}
//	};
//
//	template<class T>
//	struct greater
//	{
//		bool operator()(const T& l, const T& r) const
//		{
//			return l > r;
//		}
//	};
//}
//
//int main()
//{
//	mr::less<int> lsFunc;
//
//	cout << lsFunc(1,2) << endl;
//
//	mr::greater<int> gsFunc;
//
//	cout << gsFunc(1, 2) << endl;
//
//	return 0;
//}

