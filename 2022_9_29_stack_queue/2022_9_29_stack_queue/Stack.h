#pragma once
#include <deque>

namespace mr
{
	template<class T, class Container = deque<T>>
	class Stack
	{
	public:
		void push(const T& x)
		{
			_con.push_back(x);
		}

		void pop()
		{
			_con.pop_back();
		}

		T&  top()
		{
			return _con.back();
		}

		bool empty()
		{
			return _con.empty();
		}
		
		size_t size() const
		{
			return _con.size();
		}

	private:
		//std::vector<T> _con;
		Container _con;
	};

}