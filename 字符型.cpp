#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

int main()
{
	// 占用一个字节，存放的是字符对应的ascll码值
	char c = 'A';

	cout << c << endl;
	cout << (int)c << endl;
	cout << '\a' << endl;

	cout << "hello" << endl;
	cout << "\\" << endl;

	system("pause");

	return 0;
}