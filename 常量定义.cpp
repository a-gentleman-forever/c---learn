#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

#define Day 7

int main()
{
	//1.宏常量Day = 10;// 错误，无法修改的常量
	cout << "一周有：" << Day << "天" << endl;

	//2.const修饰的变量也称为常量
	const int month = 12;

	cout << "一年有：" << month << "个月份" << endl;


	return 0;
}