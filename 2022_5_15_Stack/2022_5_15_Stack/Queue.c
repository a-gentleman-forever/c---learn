#define  _CRT_SECURE_NO_WARNINGS 1

#include "Queue.h"

// 初始化队列 
void QueueInit(Queue* q)
{
	q->head = NULL;
	q->tail = NULL;
}
// 队尾入队列 
void QueuePush(Queue* q, QDataType data)
{
	assert(q);

	QListNode* new = (QListNode*)malloc(sizeof(QListNode));
	if (new == NULL)
	{
		perror("erro");
		exit(-1);
	}
	new->val = data;

	if (q->head == NULL)
	{
		q->head = new;
		q->tail = new;
	}
	else
	{
		q->tail->next = new;
		q->tail = new;
	}
}
// 队头出队列 
void QueuePop(Queue* q)
{
	assert(q);
	assert(!QueueEmpty(q));

	QListNode* tmp = q->head;
	q->head = q->head->next;
	free(tmp);
}
// 获取队列头部元素 
QDataType QueueFront(Queue* q)
{
	assert(q);
	assert(q->head);

	return q->head->val;
}
// 获取队列队尾元素 
QDataType QueueBack(Queue* q)
{
	assert(q);
	assert(q->tail);

	return q->tail->val;
}
// 获取队列中有效元素个数 
int QueueSize(Queue* q)
{
	QListNode* cur = q->head;
	int size = 0;

	while (cur)
	{
		size++;
		cur = cur->next;
	}

	return size;
}
// 检测队列是否为空，如果为空返回非零结果，如果非空返回0 
int QueueEmpty(Queue* q)
{
	return q->head == NULL;
}
// 销毁队列 
void QueueDestroy(Queue* q)
{
	QListNode* cur = q->head;
	QListNode* tmp = q->head;

	while (cur)
	{
		tmp = cur;
		cur = cur->next;
		free(tmp);
	}
}