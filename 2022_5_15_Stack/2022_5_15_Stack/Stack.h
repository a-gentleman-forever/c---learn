#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

typedef int StackDateType;

typedef struct Stack
{
	StackDateType* a;
	int capacity;
	int size;
}ST;

// 初始化
void StackInit(ST* ps);
// 销毁
void StackDestory(ST* ps);
// 入栈
void StackPush(ST* ps, StackDateType x);
// 出栈
void StackPop(ST* ps);
// 返回栈顶元素
StackDateType StackTop(ST* ps);
// 栈的元素个数
int StackSize(ST* ps);
// 栈是否为空
bool StackEmpty(ST* ps);