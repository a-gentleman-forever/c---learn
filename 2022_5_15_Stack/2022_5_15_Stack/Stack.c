#define  _CRT_SECURE_NO_WARNINGS 1

#include "Stack.h"

// 初始化
void StackInit(ST* ps)
{
	assert(ps);

	ps->a = (ST*)malloc(sizeof(StackDateType)*4);
	if (ps->a == NULL)
	{
		exit(-1);
	}
	ps->capacity = 4;
	ps->size = 0;
}
// 销毁
void StackDestory(ST* ps)
{
	assert(ps);

	free(ps->a);
	ps->capacity = 0;
	ps->size = 0;
}
// 入栈
void StackPush(ST* ps, StackDateType x)
{
	assert(ps);
	// 检测
	if (ps->size == ps->capacity)
	{
		StackDateType* tmp = (ST*)realloc(ps->a, sizeof(StackDateType)*ps->capacity*2);
		if (tmp == NULL)
		{
			exit(-1);
		}
		ps->a = tmp;
		
		ps->capacity *= 2;
	}

	ps->a[ps->size] = x;
	ps->size++;
}
// 出栈
void StackPop(ST* ps)
{
	assert(ps);
	assert(ps->size);

	ps->size--;
}
// 返回栈顶元素
StackDateType StackTop(ST* ps)
{
	assert(ps);

	return ps->a[ps->size - 1];
}
// 栈的元素个数
int StackSize(ST* ps)
{
	assert(ps);

	return ps->size;
}
// 栈是否为空
bool StackEmpty(ST* ps)
{
	assert(ps);

	return (ps->size == 0);
}