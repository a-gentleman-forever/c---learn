#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <ctime>
using namespace std;

int main()
{
	srand((unsigned int)time(NULL));

	int num = rand()%100;

	int get = 0;
	while (1)
	{
		cout << "请猜数字：" << endl;
		cin >> get;

		if (get < num)
		{
			cout << "猜小了" << endl;
		}
		else if (get > num)
		{
			cout << "猜大了" << endl;
		}
		else
		{
			cout << "输了" << endl;
			break;
		}
	}

	system("pause");

	return 0;
}