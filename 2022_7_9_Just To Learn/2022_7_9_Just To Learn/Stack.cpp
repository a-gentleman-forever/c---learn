#define  _CRT_SECURE_NO_WARNINGS 1

#include "Stack.h"

void StackInit(struct Stack* ps, int capacity)
{
	ps->a = (int*)malloc(sizeof(int)*capacity);

	ps->top = 0;
	ps->capacity = capacity;
}

