#pragma once

#include<iostream>
using namespace std;

struct Stack
{
	int* a;
	int top;
	int capacity;
};
// 缺省参数 只放在声明中起作用
void StackInit(struct Stack* ps, int capacity = 4);

typedef struct SeqList
{
	int* _a;
	int _size;
	int _capacity;
}SL;

void SLInit(SL& s); 
void SLPushBack(SL& s, int x);

SL& ALT(SL& s, int pos);