#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
using namespace std;

#include "Stack.h"
////缺省参数
//void Func(int a = 10)
//{
//	cout << a << endl << endl;
//}
////全缺省
//void testFunc(int a = 10, int b = 20, int c = 30)
//{
//	cout << a << endl;
//	cout << b << endl;
//	cout << c << endl << endl;
//}
////半缺省 :: 只能从右到左缺省
//void testFunc_1(int a, int b = 20, int c = 30)
//{
//	cout << a << endl;
//	cout << b << endl;
//	cout << c << endl << endl;
//}
//int main()
//{
//	Func();
//	Func(20);
//	Func(30);
//
//	testFunc();
//	testFunc(1);
//	testFunc(1,2);
//	testFunc(1,2,3);
//
//	struct Stack st1;
//	StackInit(&st1);
//
//	struct Stack st2;
//	StackInit(&st2, 100);
//
//	return 0;
//}

//int main()
//{
//	// 1、引用在定义时必须初始化
//	// 2、一个变量可以有多个引用
//	// 3、引用一旦引用一个实体、就不能引用其他实体
//	int a = 0;
//	int& b = a;// 引用：不占空间，只是一个变量的别名
//
//	cout << b << endl;
//	cout << &b << endl;
//	cout << &a << endl;
//
//	return 0;
//}
// 引用
// 1、做参数 -- 输出型参数 大对象传参、提高效率（目前:语法意义上不额外开辟空间）
// 2、做返回值 -- 返回输出型对象，调用者可以修改返回对象，减少拷贝,提高效率
	//void Swap(int& r1, int& r2)
	//{
	//	int tmp = r1;
	//	r1 = r2;
	//	r2 = tmp;
	//}
	//
	//int main()
	//{
	//	int a = 10;
	//	int b = 20;
	//
	//	Swap(a, b);// 引用
	//
	//	cout << a << " " << b << endl;
	//
	//	return 0;
	//}
//
//typedef struct SListNode
//{
//	//..
//}SListNode;
//
//void SLPushBack(SListNode*& phead, int x)
//{
//	if (phead == NULL)
//	{
//		phead = (SListNode*)malloc(sizeof(SListNode));
//	}
//}
//int main()
//{
//	SListNode* p = NULL;
//	SLPushBack(p, 1);
//	SLPushBack(p, 2);
//	SLPushBack(p, 3);
//
//	return 0;
//}

//// 引用做返回值
//
//int& Count()
//{
//	int n = 0; // 会销毁
//	//static int n = 0;// 不会销毁，才能使用引用返回
//	n++;
//
//	return n;// 值返回返回时，先拷贝n值，再返回拷贝值
//			 // 引用返回，返回对象n的别名(局部变量的引用返回是没有保证的)
//}
//
//int main()
//{
//	int& ret = Count();//n对象的空间还没被重新写入
//
//	cout << ret << endl;//cout函数重新再创建栈帧，对ret的空间重新写入
//	cout << ret << endl;
//
//	return 0;
//}
//
//void Swap(int& a, int& b)
//{
//	int tmp = a;
//	a = b;
//	b = tmp;
//}
//
//void Swap(double& a, double& b)
//{
//	double tmp = a;
//	a = b;
//	b = tmp;
//}
//
//void Swap(char& a, char& b)
//{
//	char tmp = a;
//	a = b;
//	b = tmp;
//}