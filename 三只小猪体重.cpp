#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

int main()
{
	int a = 0;
	int b = 0;
	int c = 0;

	cout << "请输入三只猪的体重：" << endl;

	cin >> a >> b >> c;

	if (a > b && b > c)
	{
		cout << "A猪最重" << endl;
	}
	else if (b > a && b > c)
	{
		cout << "B猪最重" << endl;
	}
	else
	{
		cout << "C猪最重" << endl;
	}

	system("pause");

	return 0;
}