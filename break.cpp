#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

// 使用与switch 和 循环中
int main()
{
	for (int i = 0; i < 10; i++)
	{
		if (i == 5)
		{
			break;
		}

		cout << i << endl;
	}

	system("pause");

	return 0;
}