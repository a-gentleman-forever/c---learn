#define  _CRT_SECURE_NO_WARNINGS 1

#include "Date.h"

int Date::GetMonthDays(int year, int month)
{
	int days[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	int day = days[month];

	if (month == 2
		&& ((year % 4 == 0 && year % 100 != 0)
			|| (year % 400 == 0)))
	{
		day = 29;
	}

	return day;
}

Date& Date::operator=(const Date& d)
{
	if (this != &d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}

	return *this;
}

bool Date::operator>(const Date& d)
{
	if (_year > d._year
		|| (_year == d._year && _month > d._month)
		|| (_year == d._year && _month == d._month && _day > d._day))
	{
		return true;
	}
	
	return false;
}
bool Date::operator>=(const Date& d)
{
	return (*this > d) || (*this == d);
}
bool Date::operator<(const Date& d)
{
	return !(*this > d);
}
bool Date::operator>=(const Date& d)
{
	return (*this < d) || (*this == d);
}
bool Date::operator==(const Date& d)
{
	return _year == d._year
		&& _month == d._month
		&& _day == d._day;
}
bool Date::operator!=(const Date& d)
{
	return !(*this == d);
}

Date Date::operator+(int day)
{
	Date d(*this);
	d += day;

	return d;
}
Date& Date::operator+=(int day)
{
	_day += day;
	if (_day > GetMonthDays(_year, _month))
	{
		_day -= GetMonthDays(_year, _month);
		_month++;

		if (_month == 13)
		{
			_month = 1;
			_year++;
		}
	}

	return *this;
}
Date Date::operator-(int day)
{
	Date d(*this);
	d -= day;

	return d;
}
Date& Date::operator-=(int day)
{
	_day -= day;
	if (_day <= 0)
	{
		_day += GetMonthDays(_year, _month);
		_month--;

		if (_month == 0)
		{
			_month = 12;
			_year--;
		}
	}

	return *this;
}

Date& Date::operator++()//ǰ��
{
	*this += 1;

	return *this;
}
Date Date::operator++(int)
{
	Date d(*this);
	*this += 1;

	return d;
}
Date& Date::operator--()//ǰ��
{
	*this -= 1;

	return *this;
}
Date Date::operator--(int)
{
	Date d(*this);
	*this -= 1;

	return d;
}
int Date::operator-(const Date& d)
{
	int flag = 1;
	Date max = *this;
	Date min = d;

	if (*this < d)
	{
		flag = -1;
		max = d;
		min = *this;
	}

	int n = 0;
	while (max != min)
	{
		min++;
		n++;
	}

	return flag * n;
}