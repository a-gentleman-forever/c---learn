#pragma once

#include<iostream>
using namespace std;

class Date
{
public:
	//获取月份天数
	int GetMonthDays(int year, int month);

	Date(int year = 1900, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	Date(const Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}

	void Print() const
	{
		cout << _year << "年";
		cout << _month << "月";
		cout << _day << "日" << endl;
	} 

	Date& operator=(const Date& d);

	bool operator>(const Date& d);
	bool operator>=(const Date& d);
	bool operator<(const Date& d);
	bool operator>=(const Date& d);
	bool operator==(const Date& d);
	bool operator!=(const Date& d);

	Date operator+(int day);
	Date& operator+=(int day);
	Date operator-(int day);
	Date& operator-=(int day);

	Date& operator++();//前置
	Date operator++(int);
	Date& operator--();//前置
	Date operator--(int);
	
	//日期相减
	int operator-(const Date& d);
private:
	int _year;
	int _month;
	int _day;
};
