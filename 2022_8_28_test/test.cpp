#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;


class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        int cur = 0;
        int next = cur + 1;

        while (next < nums.size())
        {
            if (nums[cur] == nums[next])
            {
                ++next;
            }
            else
            {
                ++cur;
                nums[cur] = nums[next];
                ++next;
            }
        }

        return cur + 1;
    }
};

int main()
{
	

	return 0;
}