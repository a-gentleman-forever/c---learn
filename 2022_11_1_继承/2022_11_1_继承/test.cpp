#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <string>
using namespace std;

//class Person
//{	
//public:
//	Person()
//	{
//		cout << "Person()" << endl;
//	}
//
//protected:
//	string _name;
//	string _sex;
//	int _age;
//};
//
//class Student : public Person
//{
//public:
//	Student()
//	{
//		cout << "Student()" << endl;
//	}
//
//public:
//	int _No;// 学号
//}; 
//
//int main()
//{
//	Student sobj;
//
//	// 特殊，这里虽然类型不同，但不是隐式类型转化（不会创建临时对象）
//	// 特殊支持，语法天然转换
//	//Person pobj = sobj;
//	//Person* pp = &sobj;
//	//Person& rp = sobj;
//	//
//	//int i = 0;
//	//const double& d = i;//临时变量具有常性
//
//	return 0;
//}


//class Person
//{
//public:
//	Person(int age = 10)
//	{
//		_age = age;
//		cout << "Person()" << endl;
//	}
//
//protected:
//	string _name;
//	int _age;
//};
//
//class Student : public Person
//{
//public:
//	Student(int age = 10)
//		:Person(10)
//	{
//		cout << "Student()" << endl;
//	}
//
//	void Print()
//	{
//		cout << _age << endl;
//	}
//
//
//private:
//	string _No;
//};
//
//int main()
//{
//	Student s1;
//
//	s1.Print();
//
//	return 0;
//}
//
//class Person
//{
//public:
//	string _name;
//};
//
//class Student : virtual public Person
//{
//protected:
//	int _num; // 学号
//};
//
//class Teacher : virtual public Person
//{
//protected:
//	int _id; // 职工编号
//};
//
//class Assistant : public Student, public Teacher
//{
//protected:
//	string _majorCourse; // 主修课程
//};
//// 菱形继承
//int main()
//{
//	Assistant at;
//	//	at._name = "mr";//	菱形继承的 二义性 不明确
//	// 数据冗余
//	//at.Student::_name = "张";
//	at._name = "张";
//
//	return 0;
//}

class A
{
public:
	int _a;
};

//class B : public A
class B : virtual public A
{
public:
	int _b;
};

//class C : public A
class C : virtual public A
{
public:
	int _c;
};

//class D : public B, public C
class D : virtual public B, virtual public C
{
public:
	int _d;
};

int main()
{
	D d;
	d.B::_a = 1;
	d.C::_a = 2;
	d._b = 3;
	d._c = 4;
	d._d = 5;

	return 0;
}