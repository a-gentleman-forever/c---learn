#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <String>
#include <List>
using namespace std;

void test01_String()
{
	string s1;
	//string s2("hello world");
	string s2 = "hello world"; // 隐式类型转换
	cout << s1 << endl;
	cout << s2 << endl;	

	string s3(s2);
	cout << s3 << endl;

	//string s4(s2, 6, 3);
	string s4(s2, 6);
	cout << s4 << endl;

	string s5("hello", 3);
	cout << s5 << endl;

	string s6(10, 'a');
	cout << s6 << endl;

	//cin >> s1 >> s2;
	//cout << s1 << endl;
	//cout << s2 << endl;
}

void test02_String()
{
	string s1;
	string s2("hello world");

	s1 = s2;
	s1 = "xxxxxx";
	s1 = 'y';
}

void test03_String()
{
	string s1("hello world");
	//遍历
	cout << s1[0] << endl;
	s1[0] = 'm';
	cout << s1[0] << endl;

	//s1[30]; 内部会检查越界
}
//迭代器
void test04_String()
{
	string s1("hello");
	string::iterator it = s1.begin();
	while (it != s1.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	//范围for 自动迭代结束
	//依次取s1中的每一个字符赋值给ch
	for (auto ch : s1)
	{
		cout << ch << " ";
	}
	cout << endl;

	list<int> lt(10, 1);
	list<int>::iterator lit = lt.begin();
	while (lit != lt.end())
	{
		cout << *lit << " ";
		++lit;
	}
	cout << endl;

	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;

	//范围for的底层其实就是迭代器		
}

void test05_String()
{
	string s1("hello");
	string::reverse_iterator rit = s1.rbegin();
	while (rit != s1.rend())
	{
		cout << *rit << " ";
		++rit;
	}
	cout << endl;
}

void test06_String()
{
	string s1("hello");
	s1.push_back('-');
	s1.push_back('-');
	s1.append("world");

	cout << s1 << endl; 

	string s2("我来了");
	s1 += s2;

	cout << s1 << endl;

	s1.append(++s2.begin(), --s2.end());

	cout << s1 << endl;
}

void test07_String()
{
	string s1;
	string s2("1111111");

	cout << s1.max_size() << endl;
	cout << s2.max_size() << endl;

	cout << s1.capacity() << endl;
	cout << s2.capacity() << endl;

	s1.reserve(1000);

	cout << s1.capacity() << endl;
}

int main() 
{
	//test01_String();
	//test02_String();
	//test03_String();
	//test04_String();
	//test05_String();
	//test06_String();
	test07_String();

	return 0;
}