#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

int main()
{
	int arr[10] = { 0 };

	cout << arr << endl;
	cout << sizeof(arr) / sizeof(arr[0]) << endl;
	cout << arr + 0 << endl;
	cout << arr + 1 << endl;

	return 0;
}